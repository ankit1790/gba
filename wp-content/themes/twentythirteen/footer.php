<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

</div><!-- #main -->
<footer id="colophon" class="site-footer" role="contentinfo">
    <a href="#page" class="pageup"></a>
    <div class="footer-top">
        <div class="footer-top-container">
            <div class="row">
                <?php dynamic_sidebar('sidebar-4'); ?> 
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="col-md-12">
                <div class="search-area">
                    <form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
                        <input type="text" name="s" placeholder="Search News" value="<?php echo isset($_GET['s']) ? $_GET['s'] : '' ?>"/>
                        <input type="hidden" name="post_type" value="news-events" /> <!-- // hidden 'products' value -->
                        <input type="submit" alt="Search" value="Search" />
                    </form>
                </div>
            </div>

            <?php wp_nav_menu(array('theme_location' => 'secondary', 'container_class' => 'links', 'menu_id' => 'secondary-menu')); ?>
            <?php dynamic_sidebar('copyright-box'); ?>
            <?php dynamic_sidebar('footer-social-links'); ?>
        </div>
    </div>
    <?php // get_sidebar( 'main' ); ?>


    <!--<div class="site-info">
    <?php do_action('twentythirteen_credits'); ?>
            <a href="<?php echo esc_url(__('http://wordpress.org/', 'twentythirteen')); ?>" title="<?php esc_attr_e('Semantic Personal Publishing Platform', 'twentythirteen'); ?>"><?php printf(__('Proudly powered by %s', 'twentythirteen'), 'WordPress'); ?></a>
    </div>--><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.ui.touch-punch.min.js"></script>
  <!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl-carousel/owl.carousel.js"></script>-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl-carousel/owl.carousel_new.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/parallax.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.eislideshow.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>


<script type="text/javascript">
    jQuery(function () {
        jQuery('.ei-slider').eislideshow({
            easing: 'easeOutExpo',
            titleeasing: 'easeOutExpo',
            titlespeed: 1200
        });

    });
</script>

</body>
</html>