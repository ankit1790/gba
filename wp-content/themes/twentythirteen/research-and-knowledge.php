<?php
/*
  Template Name: Research and Knowledge
 */
get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>" />
        <?php  ?>
        </div>    
        <div class="box-block">
            <div class="banner-text">
                <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php }?>
                <?php 
        if (have_posts()) {
                while (have_posts()) {  the_post(); ?>
        <h1 class="title"><?php the_title(); }}?></h1>        
            </div>    
        </div>
    </div>
</div>

<!--Start-row-->
<div class="row-block heading-block">
    <!--Start Main Container-->
    <h1><?php the_field('title'); ?></h1>
    <div class="container">
        <div class="row">
            <span><?php the_field('content-top'); ?>  
            </span>
            <div class="col-md-6">
                <div class="content-box">
                    <p><?php the_field('content-left'); ?></p>
                </div>                </div>
            <div class="col-md-6">
                <div class="content-box">
                    <p><?php the_field('content-right'); ?></p>
                </div>
            </div>
        </div>
    </div><!--end Main Container-->
</div><!--end row-->
<!--Start-row--><!--end row-->

<!--slider2-->
<div class="row-block border-block slider-area">
    <!--Start Main Container-->
    <div class="container"> 
        <!--Start row-->
        <div class="row">
            <div>
                <div class="container">
                    <div class="wrapper">
                        <div id="ei-slider3" class="ei-slider">
                            <ul class="ei-slider-thumbs">
                                <li class="ei-slider-element">Current</li>
<?php
query_posts(array('post_type' => 'op-slider2','category_name'=>'research_knowledge_slider', 'orderby' => 'date', 'order' => 'ASC','posts_per_page'=>5));
//the loop start here
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
                                <li>
                                    <a href="#"><span><img src="<?php the_field('in-active_thumbnail'); ?>" alt="thumb01" /><img src="<?php the_field('active_thumbnail'); ?>" alt="thumb01" /></span><?php the_title(); ?></a>

                                </li>
    <?php }
}
wp_reset_query();
?>
                            </ul>
                            <ul class="ei-slider-large">
<?php
query_posts(array('post_type' => 'op-slider2', 'category_name'=>'research_knowledge_slider','orderby' => 'date', 'order' => 'ASC','posts_per_page'=>5));
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
                                        <li><img src="<?php echo get_bloginfo('template_directory'); ?>/images/large/1.jpg" alt="large1" />
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="content-box">
                                                        <?php the_content(); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 float-right">
                                                    <div class="content-box">
                                                        <h2><?php the_field('col2_heading'); ?></h2>
                                                        <div class="graph-box">
                                                           <!-- <div class="graph-percent">
                                                                <h1><?php //the_field('percentage'); ?></h1>
                                                            </div>-->
                                                            <div class="col-md-12"><img src="<?php echo the_field('percentage_graph_image'); ?>"/></div>
                                                        </div>
        <?php the_field('col2_description'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
    <?php
    }
}
wp_reset_query();
?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of slider2-->



<?php get_footer(); ?> 