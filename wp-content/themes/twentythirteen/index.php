<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();
?>

<div class="home-banner">
    <div class="image-holder"><img src="<?php echo( get_header_image() ); ?>"/></div>

    <div class="text-area">
        <div class="text-wrapper">
            <div class="banner-text">  <?php dynamic_sidebar('home_page_banner_content'); ?></div>
        </div>
    </div>
</div>
<!--Start-row-->
<script type="text/javascript">
    var pieconst = false;
    var charts = [];
</script>
<div id="tab-first" class="row row-block green-row piechartblock">
    <div class="row container">
        <div class="col-md-8 column-centered align-center">
            <div class="lab-icon"><?php
                query_posts(array('category_name' => 'home-page-on-the-lab', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1));
                //the loop start here
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?><?php the_post_thumbnail('full'); ?><br>
                        <span><?php the_title(); ?></span> 
                    </div> 
                </div>
                <div class="row home-result-block">
                    <h2><?php the_field('sub_title'); ?></h2>
                    <?php the_content(); ?>
                    <?php
                }
            }
            wp_reset_query();
            ?> 
            <?php
            query_posts(array('post_type' => 'gba-results', 'category_name' => 'home-page-results', 'posts_per_page' => 4, 'orderby' => 'date', 'order' => 'ASC'));
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    ?>
                    <div class="col-md-3">
                        <figure><div id="chartContainer<?php echo get_the_ID(); ?>" style="height: 300px; width: 100%;"></div></figure>
                        <h3><?php the_field('value'); ?>%</h3>
                        <span><?php the_content(); ?></span>
                    </div>
                    <script type="text/javascript">
                        charts.push('<?php echo get_the_ID(); ?>' + ',' + '<?php the_field('value'); ?>' + ',' + '<?php urlencode(the_field('source_location')); ?>');
                    </script>
                <?php }
            }wp_reset_query();
            ?>
        </div>
    </div>
</div>



<!--end row--> 
<!--Start-row-->
<div class="row row-block">
    <div class="container">
        <div class="col-md-8 column-centered align-center">
            <div class="lab-icon"><?php
                query_posts(array('category_name' => 'home-page-benifits', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1));
                //the loop start here
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?><?php the_post_thumbnail('full'); ?><br />
                        <span><?php the_title(); ?></span>
                    </div>
                    <?php
                    the_content();
                }
            }
            wp_reset_query();
            ?>  
        </div>
        <div class="col-md-12 column-centered">
            <div class="image-grid">
                <div class="row">
                    <?php
                    $args = array('posts_per_page' => 3, 'offset' => 0, 'category_name' => 'home');

                    $myposts = get_posts($args);
                    foreach ($myposts as $post) : setup_postdata($post);
                        ?>
                        <div class="col-md-4 col-sm-4">
                            <figure class="effect-sadie"><?php the_post_thumbnail('', array('class' => 'adsasd')); ?>
                                <figcaption>
                                    <h4><?php the_title(); ?></h4>
                                </figcaption>
                            </figure>
                        </div>
                    <?php
                    endforeach;
                    wp_reset_postdata();
                    ?>


                </div>
            </div>
        </div>
    </div>
</div>
<!--end row--> 

<!-- Start Map -->
<div class="row row-block summit-slider">
    <div class="container ">
        <div class="col-md-12 column-centered align-center">
<?php dynamic_sidebar('home_page_banner_first'); ?>
        </div>           
<?php echo do_shortcode('[show-map id="1"]'); ?> 

    </div>
</div>
<!-- End Map -->

<!--Start-row-->
<div class="home-banner home-banner2">
    <div class="image-holder"><?php dynamic_sidebar('home_page_middle_banner'); ?> </div>
    <div class="text-area">
        <div class="text-wrapper">
            <div class="banner-text">
                <div class="container2">
                    <div class="col-md-8 column-left">
                        <div class="lab-icon"><?php
                            query_posts(array('category_name' => 'home-page-on-benifits-below', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1));
                            //the loop start here
                            if (have_posts()) {
                                while (have_posts()) {
                                    the_post();
                                    ?><?php the_post_thumbnail('full'); ?><br />
                                    <span><?php the_title(); ?></span> </div>
                                <?php the_content(); ?> <?php
                                }
                            }
                            wp_reset_query();
                            ?>  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end row--> 
<!--Start-row-->
<div class="row row-block">
    <div class="col-md-12">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Deleted Building solutions together -->    
                    <h1>Latest News</h1>
                    <?php
                    $args = array('post_type' => 'news-events', 'category_name' => 'current-news', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 3);
                    $loop = new WP_Query($args);

                    while ($loop->have_posts()) {
                        $loop->the_post();
                        ?>

                        <div class="col-md-4 align-center">
                            <figure class="news-square-border">
                                <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>"/>
                            </figure>
                            <div class="content-box">
                                <h3><?php the_title(); ?></h3>
                                <p><?php the_field('short_description'); ?></p>
                            </div>
                        </div>

<?php } ?>
                </div>
                <div class="col-md-12 align-center">
                    <span class="button-holder">
                        <a href="<?php echo home_url(); ?>/news" class="btn-2 expand">Read More</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end row-->
    <?php get_sidebar(); ?>
<!--Start-row-->
<div class="row-block twitter-block border-block summit-slider">
    <!--Start Main Container-->
<?php dynamic_sidebar('sidebar-3'); ?> 
</div><!--end row-->

<!--Start-row-->
<div class="row-block links-block">
    <!--Start Main Container-->
    <div class="container">
        <div class="row">
            <h1>Our Sponsors</h1>
            <div id="sponsors-slider" class="owl-carousel" style="padding: 0 35px">
<?php dynamic_sidebar('sidebar-5'); ?>            	
            </div>
        </div>
    </div><!--end Main Container-->
</div>
<style type="text/css">
    #sponsors-slider .owl-prev {
        left: 0;
        position: absolute;
        top: 55px;
    }
    #sponsors-slider .owl-next {
        position: absolute;
        right: 0;
        top: 55px;
    }
    #sponsors-slider .owl-item img{width: auto;display: inline-block}
</style>
<?php get_footer(); ?>