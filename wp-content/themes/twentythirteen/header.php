<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/grid.css" media="all">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/slider.css" media="all">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/vault.css" media="all">
<!--<link href="<?php //echo get_bloginfo('template_directory'); ?>/css/owl-carousel/owl.carousel.css" rel="stylesheet">-->
<!--<link href="<?php //echo get_bloginfo('template_directory'); ?>/css/owl-carousel/owl.theme.css" rel="stylesheet">-->
<link href="<?php echo get_bloginfo('template_directory'); ?>/css/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="<?php echo get_bloginfo('template_directory'); ?>/css/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/vault.css" media="all">
<link href='http://fonts.googleapis.com/css?family=Ek+Mukta:200,300,400,500,600' rel='stylesheet' type='text/css'>
<noscript>
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/noscript.css" media="all">
</noscript>
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
<?php wp_head(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.canvasjs.min.js"></script>
<script type="text/javascript">
    jQuery(function(){
        if(window.location.hash){
            jQuery('html, body').animate({
                scrollTop: jQuery(window.location.hash).offset().top - 160
            }, 4000);
        }
    });
</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
<header>
<div id="navbar" class="navbar">
  <div class="logo-area"><div class="logo"><a href="<?php echo get_option("siteurl"); ?>"><img src="<?php echo get_bloginfo('template_directory'); ?>/images/logo.png"></a></div></div>
<div class="top-menu-area">
     <div class="navbar-header">
    </div>

<?php wp_nav_menu( array( 'theme_location' => 'topmenu','container_id'=>'bs-example-navbar-collapse-1', 'container_class'=>'collapse navbar-collapse','menu_class' => 'nav navbar-nav', 'menu_id' => 'top-menu' ) ); ?>
</div>

  <div class="main-navigation-area">
  <nav id="site-navigation" class="navigation main-navigation" role="navigation">
            <button class="menu-toggle">
            <?php _e( 'Menu', 'twentythirteen' ); ?>
            </button>
            <a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>">
            <?php _e( 'Skip to content', 'twentythirteen' ); ?>
            </a>
            <div>
            <?php
			
				$defaults = array(
					'theme_location'  => 'primary',
					'menu_id' 		  => 'primary-menu',
					'container'       => 'div',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'nav-menu',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 0,
					'walker'          => new Child_Wrap()
				);
				wp_nav_menu( $defaults );
			?>
              <?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu' ) ); ?>
              
              <?php echo '<div class="mobile-top-menu">';wp_nav_menu( array( 'theme_location' => 'topmenu','container_id'=>'bs-example-navbar-collapse-1', 'container_class'=>'collapse navbar-collapse','menu_class' => 'nav navbar-nav','walker'=> new Child_Wrap(), 'menu_id' => 'top-menu' ) );
			echo '</div>';
            ?>
            </div>
            <!--<?php// get_search_form(); ?>--> 
          </nav>
         </div>
</div>
<!-- #navbar -->
</header>
<div id="main" class="site-main">
