<?php get_header(); ?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
<?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
            <?php endif; ?>

        </div>
        <div class="box-block">
            <div class="banner-text">
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    the_title();
                }
            }
            wp_reset_query();
            ?></h1>        
            </div>    
        </div>

    </div>
</div>
<div class="row-block heading-block">
    <div class="container">
        <!--Start row-->
        <div class="row">

            <div class="col-md-12">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>  <?php
                        the_content();
                        ?>
                    </div>
                </div>
                
                <?php
            endwhile;
        endif;
        wp_reset_query();
        ?>
    </div>
</div>

<div class="row heading-block">
    <div class="container">
    <?php 
            $year = get_field('year');
            $categoryName = $year.'_summit';
            $args = array('post_type'=>'summit_panel','category_name'=>$categoryName,'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 300);
            $loop = new WP_Query( $args );
            //the loop start here
            $i=0;
            while ( $loop->have_posts() ) { $loop->the_post();
            $i++;if($i%3==1) echo '<div class="row">';
            ?>
                    <div class="col-md-4">
                        <figure class="square-border">
                        <?php if(get_field('left_image')):?>
                        <img src="<?php echo the_field('left_image'); ?>"/>
                        <?php endif;?>
                        </figure>
                        <div class="content-box align-center">
                            <?php the_title('<h2>','</h2>'); 
                                  the_field('short_description');
                            ?>
                            <span><a href="<?php the_permalink();?>" class="button">Read More</a></span>
                        </div>
                    </div>
           <?php
           if(($i%3==0) || (($loop->current_post)+1 == count($loop->posts))) echo '</div>';
            }
        wp_reset_postdata();?>
    </div>
</div>


<div class="row-block views-block border-block">
    <div class="container">
        <div class="row">
            <h1><?php the_field('scrolling_quote_head');?></h1>
            <div id="peer-users" class="owl-carousel">

                <?php
                query_posts(array('post_type' => 'peer-learning-users','category_name'=>'scrolling-quote-'.$year.'-summit'));
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <div class="item">
                            <?php the_content();
                            the_field('name');
                            ?>
                            <figure>
                                <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                            </figure>
                            <h5><?php the_title(); ?></h5>
                            <span><?php the_field('sub_title'); ?></span>
                        </div>
                        <?php
                    }
                }
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
</div><!--end row--> 


<?php get_footer(); ?>