<?php

get_header(); ?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
           <?php if (has_post_thumbnail()) : ?>
            <?php the_post_thumbnail('full'); ?>
            <?php endif; ?>
            <?php if (!has_post_thumbnail()) : ?>
               <img src="<?php echo get_bloginfo('template_directory'); ?>/images/bank-profile-banner.jpg"/>
<?php endif; ?>
            
        </div>
            <div class="box-block">
            <div class="banner-text">
                    
            </div>    
        </div>
    </div>
</div>
<div class="row-block heading-block">
    <div class="container">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();?>
        <div class="row">
            <?php
                    the_title('<h1>','</h1>');
                    the_content();
            ?>
        </div>
        <!--Start row-->
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">
                    <?php the_field('description') ?>
                </div>
            </div>
        </div> 
        <?php 
        endwhile;
        endif; 
        wp_reset_query();
        ?>
    </div>
</div>
<?php get_footer(); ?>