function getQueryStrings() {
    var assoc = {};
    var decode = function (s) {
        return decodeURIComponent(s.replace(/\+/g, " "));
    };
    var queryString = location.search.substring(1);
    var keyValues = queryString.split('&');

    for (var i in keyValues) {
        var key = keyValues[i].split('=');
        if (key.length > 1) {
            assoc[decode(key[0])] = decode(key[1]);
        }
    }
    return assoc;
}


jQuery(document).ready(function () {
    if (jQuery("#peer-users").length) {
        jQuery("#peer-users").owlCarousel({
            loop: true,
            autoplay: 3000,
            items: 1
        });
    }
    jQuery("#cust-insights-left,#cust-insights-right").owlCarousel({
        autoplay: 1000,
        loop: true,
        items: 1,
        nav: true,
        navText: ["<i class='icon-chevron-left icon-white'></i>", "<i class='icon-chevron-right icon-white'></i>"],
        dots: false
    });
    jQuery("#journey-slider").owlCarousel({
        autoplay: 1000,
        dots: false,
        nav: true,
        items: 4,
        loop: true,
        navText: ["<i class='icon-chevron-left'></i>", "<i class='icon-chevron-right'></i>"],
        /*itemsDesktop : [1199,1],
         itemsDesktopSmall : [979,1]*/
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 3
            },
            1281: {
                items: 4
            }
        }
    }).on('translated.owl.carousel', function (event) {
        jQuery(".journey-block-slider .owl-carousel .owl-stage .owl-item").each(function () {
            jQuery(this).removeClass('blank');
        });
        jQuery(".journey-block-slider .owl-carousel .owl-stage div.active:first").addClass("blank");
        jQuery(".journey-block-slider .owl-carousel .owl-stage div.active:last").next().addClass("blank");
    });
    jQuery(".journey-block-slider .owl-carousel .owl-stage div.active:first").addClass("blank");
    jQuery(".journey-block-slider .owl-carousel .owl-stage div.active:last").next().addClass("blank");

    /*var qs = getQueryStrings();
     var myParam = qs["top"];
     if (jQuery('#' + myParam).length) {
     var top = parseInt(jQuery('#' + myParam).offset().top) - 350;
     jQuery('html,body').animate({
     scrollTop: top
     }, 2000);
     return false;
     }*/
});

//to render pieChart
function renderPieChart(id,val,source){
        CanvasJS.addColorSet("chartShades",["#2c3a69","#fc5721"]);
	var ChartID = new CanvasJS.Chart("chartContainer"+id,
	{
		animationEnabled: true,
                colorSet: "chartShades",
                backgroundColor : "transparent",
                toolTip:{
                  //content: toString(content),
                  contentFormatter: function (e) {
                      var content = source==''? e.entries[1].dataPoint.y:source;
                      return content;
                  }
                },
		legend: {
			verticalAlign: "bottom",
			horizontalAlign: "center"
		},
		theme: "theme1",
		data: [
		{        
			type: "pie",
			indexLabelFontFamily: "Ek Mukta,sans-serif",       
			indexLabelFontSize: 20,
			indexLabelFontWeight: "bold",
			startAngle:0,
			indexLabelFontColor: "MistyRose",       
			indexLabelLineColor: "darkgrey", 
			indexLabelPlacement: "inside", 
			//toolTipContent: "{y}%",
			showInLegend: false,
			indexLabel: "#percent%", 
			dataPoints: [
				{  y: val},
				{  y: 100-val}
			]
		}
		]
	});
	ChartID.render();
        return ;
}

jQuery(window).on('scroll',function() {
    if(jQuery('.piechartblock').length){
        var scrolltop = jQuery(this).scrollTop()+200;
        var destination = jQuery('.piechartblock').offset().top;
            if(scrolltop >= destination && !pieconst) {
                pieconst = true;
                for (var k = 0; k < charts.length; k++) { 
                    var valArr = charts[k].split(',');
                    renderPieChart(parseInt(valArr[0]),parseFloat(valArr[1]),valArr[2]);
                }
        }
    }
});