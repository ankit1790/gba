<?php
/*
  Template Name: Get Educated
 */
get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
            <?php $bannerImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
            if($bannerImage){?><img src="<?php echo $bannerImage?>"/>
            <?php }else{?><img src="<?php echo get_bloginfo('template_directory'); ?>/images/bank-profile-banner.jpg"/><?php }?>
        </div>
          <div class="box-block">
            <div class="banner-text">
                <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php }?>
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {  the_post(); ?><?php the_title(); ?>
                <?php  }}?>
                </h1>        
            </div>    
        </div>
    </div>
</div>
<div class="row-block heading-block">
    <div class="container">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();?>
        <div class="row">
            <?php   the_title('<h1>','</h1>');?>
            <span><?php the_content();?></span>
        </div>
        <!--Start row-->
        <div class="row">
            <div class="col-md-7">
                <div class="content-box">
                    <?php the_field('left_content') ?>
                </div>
            </div>
            <div class="col-md-4 float-right">
                <div class="right-block">
                    <?php the_field('right_content') ?>
                </div>
            </div>
        </div> 
        <?php 
        endwhile;
        endif; 
        wp_reset_query();
        ?>
    </div>
</div>



<?php get_footer(); ?> 