<?php
/*
  Template Name: Tactical Research
 */
get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image"><?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
<?php endif; ?></div>    
        <div class="box-block">
            <div class="banner-text">
                <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php }?>
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {  the_post(); ?><?php the_title(); ?></h1>        
            </div>    
        </div>
    </div>
</div>
<div class="row-block heading-block" id="customer-insights">
    <!--Start Main Container-->
    <div class="container">
        <!--Start row-->
        <div class="row">
            <h1><?php the_field('sub_head'); ?></h1>
            <span><?php the_content();?></span>
           <?php  }}?>
            <div class="col-md-6">
                
                <div class="content-box">
<?php the_field('left_top'); ?>
                    <div class="slider-wrapper">
                        <div id="cust-insights-left" class="owl-carousel">
                        <?php
                                query_posts(array('post_type' => 'cust_insight_posts','category_name'=>'tactical-insight-left'));
                                if (have_posts()) {
                                    while (have_posts()) {
                                        the_post();
                                        ?>
                                        <div class="item">
                                            <div class="graph-box">
                                                <div class="graph-percent"><h1><?php the_field('value')?>%</h1></div>
                                                <div class="graph-chart"><?php the_content();?></div>
                                            </div>
                                        </div>
                                <?php
                                } }
                            wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
                <div class="content-box">
<?php the_field('left_bottom'); ?>
                </div>
            </div><!--col-md-7-->

            <div class="col-md-6">
                <div class="content-box">
<?php the_field('right_top'); ?>
                </div>
                <div class="content-box">
<?php the_field('right_bottom'); ?>
                    <div class="slider-wrapper">
                        <div id="cust-insights-right" class="owl-carousel">
                        <?php
                                query_posts(array('post_type' => 'cust_insight_posts','category_name'=>'tactical-insight-right'));
                                if (have_posts()) {
                                    while (have_posts()) {
                                        the_post();
                                        ?>
                                        <div class="item">
                                            <div class="graph-box">
                                                <div class="graph-percent"><h1><?php the_field('value')?>%</h1></div>
                                                <div class="graph-chart"><?php the_content();?></div>
                                            </div>
                                        </div>
                                <?php
                                } }
                            wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div><!--end-col-md-4-->
        </div><!--end row-->
    </div>
</div><!--end row--> 


<style type="text/css">
    .owl-theme .graph-chart > p {
        margin: 35px 0 0;
    }
</style>



<?php get_footer(); ?> 