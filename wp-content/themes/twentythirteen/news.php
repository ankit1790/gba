<?php
/*
  Template Name: News
 */
get_header();
?>
<div class="intro-banner">
    <div class="container">
        <h3><?php the_title(); ?></h3>        
    </div>
</div>
<?php wp_reset_query();?>
<div class="row-block news-page heading-block">
    <div class="container">
            <?php 
            if ( get_query_var('paged') ) {
            $paged = get_query_var('paged');
            } else if ( get_query_var('page') ) {
                $paged = get_query_var('page');
            } else {
                $paged = 1;
            }
            $args = array('post_type'=>'news-events','category_name'=>'current-news','orderby' => 'date', 'order' => 'DESC','posts_per_page'=>12,'paged'=>$paged);
            $loop = new WP_Query( $args );
            //the loop start here
            $i=0;$article=0;
            while ( $loop->have_posts() ) { $loop->the_post();
            $article +=1;
            if($article % 3 == 1) echo '<div class="row">';?>
                
                <div class="col-md-4 align-center">
                    <figure class="news-square-border">
                        <?php $image= wp_get_attachment_url(get_post_thumbnail_id($post->ID)); 
                         if($image)
                         { ?>
                        <img src="<?php echo $image; ?>"/>
                        <?php  } ?>
                    </figure>
                    <div class="content-box">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_field('short_description'); ?></p>
			<?php if(get_field('other_link')){?>
			<span><a target="_blank" href="<?php the_field('other_link');?>" class="button">Read More</a></span>
			<?php }else{?>
                        <span><a target="_blank" href="<?php the_permalink();?>" class="button">Read More</a></span>
			<?php }?>
                    </div>
                </div>
            <?php if($article % 3 == 0 || $loop->current_post+1 == count($loop->posts)) echo '</div>';
             
            }?>

        <div class="row row-block post-pagination align-center">
            <div class="col-md-12">
        <?php
            wp_pagenavi( array( 'query' => $loop ) );
            wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>