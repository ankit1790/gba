<?php
/*
  Template Name: Become Amassador
 */
get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
            <?php
            if (has_post_thumbnail()) :
                the_post_thumbnail('full');
            endif;
            ?>
        </div>
         <div class="box-block">
            <div class="banner-text">
                <?php 
                   if( get_field('banner_first_head') ):  ?>
                <span> <?php the_field('banner_first_head') ?></span>
                   <?php endif; ?>
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {  the_post(); ?><?php the_title(); ?>
                <?php  }}?>
                </h1>        
            </div>    
        </div>
    </div>
</div>

<div class="row-block heading-block">
    <!--Start Main Container-->
    <div class="container">
        <!--Start row-->
        <div class="row">
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
                    the_title('<h1>', '</h1>');?>
                    <span><?php the_content();?></span>
                <?php endwhile;
            endif;
            ?>
        </div><!--end row-->

        <!--Start row-->
        <div class="row">
            <div class="col-md-6">
                <div class="content-box">
<?php the_field('left_column'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-box">
<?php the_field('right_column'); ?>
                </div>
            </div>
        </div><!--end row--> 
    </div>	<!--end Main Container-->
</div><!--end row--> 
<?php wp_reset_query(); ?>

<!--Start-row-->
<div class="row-block result-block">
    <!--Start Main Container-->
    <div class="container">
        <h2>THE RESULTS</h2>
        <div class="row">
            <ul>
                <?php
                query_posts(array('category_name' => 'become-ambassodor', 'post_type' => 'gba-results', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 4));
                //the loop start here
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <li>
                            <h3> <?php the_field('value'); ?></h3>
                            <span><?php the_content(); ?></span>
                        </li>
                        <?php
                    }
                }
                wp_reset_query();
                ?>         
            </ul>
        </div>
    </div><!--end Main Container-->
</div><!--end row-->          
<?php get_footer(); ?> 