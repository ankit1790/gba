<?php get_header(); ?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
<?php $bannerImage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
if ($bannerImage) {
    ?><img src="<?php echo $bannerImage ?>"/>
            <?php } else { ?><img src="<?php echo get_bloginfo('template_directory'); ?>/images/bank-profile-banner.jpg"/><?php } ?>
        </div>

    </div>
</div>
<div class="row-block heading-block">
    <div class="container">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="row">
                <?php the_title('<h1>', '</h1>'); ?>
                    <span><?php the_content(); ?></span>
                </div>
                <!--Start row-->
                <div class="row">
                    <div class="col-md-7">
                        <div class="content-box">
        <?php the_field('left_description') ?>
                        </div>
                    </div>
                    <div class="col-md-4 float-right">
                        <div class="right-block">
        <?php the_field('at_a_glance') ?>
                        </div>
                    </div>
                </div> 
        <?php
    endwhile;
endif;
wp_reset_query();
?>
    </div>
</div>
<!--Start-row-->
<?php if (get_field('first_result_value') || get_field('first_result_description') || get_field('second_result_value') || get_field('second_result_description') || get_field('third_result_value') || get_field('third_result_description') || get_field('fourth_result_value') || get_field('fourth_result_description')): ?>

    <div class="row-block result-block">
        <!--Start Main Container-->
        <div class="container">
            <h2><?php the_field('results_title') ?></h2>
            <div class="row">
                <ul><?php ?> 
                    <li>
                        <h3> <?php the_field('first_result_value'); ?></h3>
                        <span><?php the_field('first_result_description'); ?></span>
                    </li>
                    <li>
                        <h3> <?php the_field('second_result_value'); ?></h3>
                        <span><?php the_field('second_result_description'); ?></span>
                    </li>
                    <li>
                        <h3> <?php the_field('third_result_value'); ?></h3>
                        <span><?php the_field('third_result_description'); ?></span>
                    </li>
                    <li>
                        <h3> <?php the_field('fourth_result_value'); ?></h3>
                        <span><?php the_field('fourth_result_description'); ?></span>
                    </li>         
                </ul>
            </div>
        </div><!--end Main Container-->
    </div>
<?php endif; ?>
<!--end row-->
<?php if (get_field('quote_description') || get_field('quote_profile_picture') || get_field('quote_profile_name') || get_field('quote_profile_sub_title')): ?>
    <div class="row-block views-block border-block">
        <div class="container">
            <div class="row">

    
                <h1><?php the_field('quote_title') ?></h1>
                <?php the_field('quote_description') ?>
                <figure>
                    <img src="<?php the_field('quote_profile_picture'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                </figure>
                <h5><?php the_field('quote_profile_name'); ?></h5>
                <span><?php the_field('quote_profile_sub_title'); ?></span>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--end row--> 
<div class="row-block social-block links-block">
    <!--Start Main Container-->
    <div class="container">
        <div class="row align-center">
            <div class="col-md-12">

<?php if (get_field('facebook_icons') || get_field('twitter_icons') || get_field('linkedin_icons') || get_field('google+_icons') || get_field('youtube_icons')): ?>
                    <div class="sfmsb-follow-social-buttons">
                        <h2 class="widgettitle">Follow <?php the_title(); ?></h2>
                        <a href="<?php the_field('facebook_url') ?>" target="_blank"><img src="<?php the_field('facebook_icons') ?>" alt=""></a>
                        <a href="<?php the_field('twitter_url') ?>" target="_blank"><img src="<?php the_field('twitter_icons') ?>" alt=""></a>
                        <a href="<?php the_field('linkedin_url') ?>" target="_blank"><img src="<?php the_field('linkedin_icons') ?>" alt=""></a>
                        <a href="<?php the_field('google+_url') ?>" target="_blank"><img src="<?php the_field('google+_icons') ?>" alt=""></a>
                        <a href="<?php the_field('youtube_url') ?>" target="_blank"><img src="<?php the_field('youtube_icons') ?>" alt=""></a>
                    </div>
<?php endif; ?>


                <?php dynamic_sidebar('social-media-share'); ?>
            </div>
        </div>
    </div><!--end Main Container-->
</div>
<?php get_footer(); ?>