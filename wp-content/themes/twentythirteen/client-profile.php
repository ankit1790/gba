<?php
/*
Template Name: My Client Profile
*/
get_header();
?>

<div><img src="<?php the_field('hero_image'); ?>"/></div>
<div class="row-block heading-block">
  <!-- Start Main Container -->
  <div class="container">
    <!--Start row-->
    	<div class="row profile-content-blk">
        	<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
			<?php endwhile; ?>
     </div>
	 <!--end row-->
        
         <!--Start row-->
        <div class="row-block">
        	<div class="col-md-7">
            	<div class="content-box">
            		<h2>About Helene</h2>
                		<p><?php the_field('about_helene'); ?></p>
                </div><!--end content-box-->
                
                <div class="content-box">
            		<h2>About the Award</h2>
                		<p><?php the_field('about_the_award'); ?></p>
                </div><!--end content-box-->
            </div><!--col-md-8-->
            
            
            <div class="col-md-4 float-right">
            	<div class="right-block">
            		<h4>At a Glance</h4>
              <h1><?php the_field('at_a_glance_heading'); ?></h1>
                	<p><?php the_field('at_a_glance_description'); ?></p>
                                		<h4><?php the_field('accolades_heading'); ?></h4>
               	  <p><?php the_field('accolades_heading_description'); ?></p>
                	
			  </div>
            </div><!--end-col-md-4-->
        </div><!--end row--> 
	</div>	<!--end Main Container-->
    </div><!--end row--> 
    
 
 <!--Start-row-->
	<div class="row-block gallery-block">
    <!--Start Main Container-->
		<div class="container">
<?php if (get_field('video') && get_field('video') != "") { ?>
<iframe  width="800" height="420" src="<?php echo the_field('video'); ?>" frameborder="0" allowfullscreen></iframe>
<?php } else{ ?>
        	<ul class="gallery-list">
            	<li>
                <figure>
            	  <img src="<?php the_field('small_image'); ?>">
                 </figure>
                </li>
            	<li>
                 <figure>
            	 <img src="<?php the_field('small_image'); ?>">
                 </figure>
            	</li>
            	<li>
                <figure>
            	 <img src="<?php the_field('small_image'); ?>">
                 </figure>
            	</li>
            	<li>
                <figure>
            	  <img src="<?php the_field('medium_image'); ?>">
                 </figure>
            	</li>
                 <li>
                <figure>
            	<img src="<?php the_field('medium_image'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>

            	 <img src="<?php the_field('small_image'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	 <img src="<?php the_field('small_image'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	  <img src="<?php the_field('small_image'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	 <img src="<?php the_field('small_image'); ?>">
                 </figure>
            	</li>

                </ul>
<?php } ?>
        	</div><!--end Main Container-->
        </div>
<?php get_footer(); ?>