<?php
/*
  Template Name: Knowledge Products
 */
get_header();
?>
<!--Start row-->    	
        <div class="row">
            <div class="full-image-area">
                <div class="full-image"><?php if (has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail('full'); ?>
                    <?php endif; ?>
                </div>
          <div class="box-block">
            <div class="banner-text">
                <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php }?>
                <h1 class="title"><?php the_title();?></h1>        
            </div>    
        </div>
            </div>
        </div>
 
        <!--end row--> 
        <div class="row-block heading-block" style="padding-bottom: 0px">
            <h1><?php the_field('intro_head'); ?></h1>
            <div class="container">
                <div class="row">
                    <span><?php the_field('intro_text'); ?></span>
                </div>
            </div>
        </div>
        

          
<div class="heading-block">
    <!--Start Main Container-->
    <div class="container">
            <?php
            $args = array('post_type'=>'knowledge_product','orderby' => 'date', 'order' => 'DESC','posts_per_page'=>300);
            $loop = new WP_Query( $args );
            //the loop start here
            $i=0;
            while ( $loop->have_posts() ) { $loop->the_post();
            $i++;if($i%3==1) echo '<div class="row">';
            ?>
                    <div class="col-md-4">
                        <figure class="square-border"><img class="full-image" src="<?php echo the_field('avatar'); ?>"/></figure>
                        <div class="content-box align-center">
                            <h2><?php echo the_title(); ?></h2>
                            <?php the_content();?>
                        </div>
                    </div>
           <?php
           if($i%3==0 || $loop->current_post+1 == count($loop->posts)) echo '</div>';
            }
        wp_reset_postdata();?>
        
    </div>	<!--end Main Container-->
</div>

<?php get_footer(); ?>