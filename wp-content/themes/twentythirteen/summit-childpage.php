<?php
/*
Template Name: Summit Child Page
*/


get_header(); ?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
        <?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
<?php endif; ?>
            
        </div>
		<div class="box-block">
            <div class="banner-text">
			<!-- <span>GBA For Women</span> -->
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {  the_post(); //the_title();
				}}?></h1>        
            </div>    
        </div>
        
    </div>
</div>

<div class="row intro-banner">
    <div class="container">
        <div class="col-md-12">
            <ul class="greenbar-menu">
                <li><a href="<?php echo get_permalink(1056); ?>">About</a></li> 
                <li><a href="<?php echo get_permalink(1058); ?>">Agenda</a></li>
                <li><a href="<?php echo get_permalink(1060); ?>">Speakers</a></li>
                <li><a href="<?php echo get_permalink(1062); ?>">Sponsors</a></li>
                <li><a href="<?php echo get_permalink(1064); ?>">Location</a></li>
                <li><a href="<?php echo get_permalink(1066); ?>">Register</a></li>
                <li><a href="<?php echo get_permalink(1068); ?>">Previous Summits</a></li>
            </ul>
        </div>
    </div>
</div>	

<div class="row-block heading-block">
    <div class="container">
        
        <div class="row">
           
        </div>
        <!--Start row-->
        <div class="row">
            <div class="col-md-12">
             <?php
             if (have_posts()) : while (have_posts()) : the_post();?>  <?php
                  
                the_content();
               ?>
              <?php 
                 endwhile;
                 endif; 
                 wp_reset_query();
                 ?>  
       			 </div>
            </div> 
		  <?php //the_field('short_description'); ?> 
        <?php 
       
        ?>
    </div>
</div>
<!--Start-row-->
 <?php $id=get_the_ID();
if($id=='1056') {
 ?> 
 
  <!--Start-row-->
	<div class="row-block gallery-block">
    <!--Start Main Container-->
		<div class="container">
<?php if (get_field('video') && get_field('video') != "") { ?>
<iframe  width="800" height="420" src="<?php echo the_field('video'); ?>" frameborder="0" allowfullscreen></iframe>
<?php } else{ ?>
               <?php 
                   if( get_field('small_image') ):  ?>
        	<ul class="gallery-list">
            	<li>
                <figure>
            	  <img src="<?php the_field('small_image'); ?>">
                 </figure>
                </li>
            	<li>
                 <figure>
            	 <img src="<?php the_field('small_image1'); ?>">
                 </figure>
            	</li>
            	<li>
                <figure>
            	 <img src="<?php the_field('small_image2'); ?>">
                 </figure>
            	</li>
            	<li>
                <figure>
            	  <img src="<?php the_field('medium_image'); ?>">
                 </figure>
            	</li>
                 <li>
                <figure>
            	<img src="<?php the_field('medium_image1'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>

            	 <img src="<?php the_field('small_image3'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	 <img src="<?php the_field('small_image4'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	  <img src="<?php the_field('small_image5'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	 <img src="<?php the_field('small_image6'); ?>">
                 </figure>
            	</li>

                </ul><?php endif; ?>
<?php } ?>
        	</div><!--end Main Container-->
        </div>
		
<div class="row-block result-block">
    <!--Start Main Container-->
    <div class="container">
        <h2><?php the_field('the_results_title'); ?></h2>
        <div class="row">
            <ul><?php
        ?> <?php
                query_posts(array('category_name' => 'the-results-for-summit-about', 'post_type' => 'gba-results', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 4));
                //the loop start here
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                     ?>
                        <li>
                            <h3> <?php the_field('value'); ?></h3>
                            <span><?php the_content(); ?></span>
                        </li>
                        <?php
                    }
                }
                wp_reset_query();
                ?>         
            </ul>
        </div>
    </div><!--end Main Container-->
</div><!--end row-->
<?php } else {} ?>


<?php get_footer(); ?>