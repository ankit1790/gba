<?php
/*
  Template Name: Contact Us
 */
get_header();
?>
<div class="intro-banner">
    <div class="container">
        <h3><?php the_title(); ?></h3>        
    </div>
</div>
<div class="container contact-us">
    <div class="row row-block">
        <div class="col-md-8">
            <h2>Get in touch with us</h2>
            <?php echo do_shortcode('[contact-form-7 id="480" title="Contact form 1"]'); ?>
        </div>
        <div class="col-md-4">
            <?php dynamic_sidebar('contact-details'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>