<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
get_header();
?>

<!--<div id="masthead" class="site-banner mobile-hidden" role="banner"><img src="<?php echo get_bloginfo('template_directory'); ?>/images/home-slider.jpg"><a class="home-link" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
  <h1 class="site-title">
<?php bloginfo('name'); ?>
  </h1>
  <h2 class="site-description">
<?php bloginfo('description'); ?>
  </h2>
  </a> </div>-->

<?php echo do_shortcode("[huge_it_slider id='2']"); ?>

<!-- #masthead -->

<div class="intro-banner">
    <div class="container">
        <h3>Our Mission</h3><p>Propel the growth of women in business and women's wealth creation, while<br>
            generating superior business outcomes for member financial institutions.</p>
    </div>
</div>

<!--Start-row-->
<div class="row-block">
    <!--Start Main Container-->
    <div class="container">
        <div class="row">
            <div class="col-md-7">

                <?php
                if (shortcode_exists('video')) {
                    $pattern = get_shortcode_regex();
                    preg_match('/' . $pattern . '/s', get_post_field('post_content', 55), $matches);
                    if (is_array($matches) && $matches[2] == 'video') {
                        $shortcode = $matches[0]; // get shortcode with it's parameters
                        echo '<figure class="video-wrapper" id="video-55">' . do_shortcode($shortcode) . '</figure>'; // execute that shortcode we've found
                    } else {
                        echo '<figure><a href="';
                        the_permalink();
                        echo '" class="thumbnail-wrapper">';
                        echo the_post_thumbnail('large');
                        echo '</a></figure>';
                    }
                } else {
                    echo '<figure><a href="';
                    the_permalink();
                    echo '" class="thumbnail-wrapper">';
                    echo the_post_thumbnail('large');
                    echo '</a></figure>';
                }
                ?>
            </div>
            <div class="col-md-5 video-content">
                <div class="content-box">
                    <h2> <?php echo get_the_title(55); ?></h2>
                    <p><?php
                        //echo get_post_field('post_content', 55);
                        echo strip_shortcodes(get_post_field('post_content', 55));
                ?></p>
                    <span><a id="post-video-55" href="javascript:void(0)" onclick="startVideo('55', this);">Play Video</a></span>
                </div>
            </div>
        </div>
    </div><!--end Main Container-->
</div>
<!--end row--> 

<div class="row-block">
    <div id="parallax-3" class="parallax">

        <div class="container">
            <div class="pull-right"><span>MEET</span>
                <h1 class="title">Helene Nasr</h1>
                <p>Advocate for safe use of nuclear technology in the Middle East, more than 20 years' experience in nuclear energy field.</p>
                <button>Learn More</button>
            </div>
        </div>
    </div>    
</div>









<!--Start-row-->
<div class="row-block">
    <!--Start Main Container-->
    <div class="container">
        <h1>In the News</h1>
        <div class="row">
            <div class="col-md-4">
                <div class="content-box">
                    <h3>Bank al Etihad Shares Insights on New Women's Market Program</h3>
                    <p>Bank al Etihad of Jordan recently announced the launch of its Shorouq Program, which offers a wide variety of financial and non-financial services for women. The bank's General Manager, Nadia Al Saeed, offered… </p>
                    <span><a href="#">Read More</a></span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="content-box">
                    <h3>Global Banking Alliance for Women Announces Partnership to Close Gender Gap</h3>
                    <p>The Global Banking Alliance for Women announces a partnership with the Inter-American Development Bank and Data2X to expand the collection and use…</p>
                    <span><a href="#">Read More</a></span>
                </div>
            </div>

            <div class="col-md-4">
                <div class="content-box">
                    <h3>Alliance Sponsor IFC helps GBA Member Bank al Etihad Extend Services to Women</h3>
                    <p>IFC is helping Bank al Etihad launch a new banking model that caters to women in Jordan, with a particular focus on small and medium enterprises owned by women. </p>
                    <span><a href="#">Read More</a></span>
                </div>
            </div>
        </div>
    </div><!--end Main Container-->
</div><!--end row-->

<!--Start-row-->
<div class="row-block twitter-block border-block">
    <!--Start Main Container-->
<?php dynamic_sidebar('sidebar-3'); ?> 
</div><!--end row-->



<!--Start-row-->
<div class="row-block links-block">
    <!--Start Main Container-->
    <div class="container">

        <h1>Our Sponsors</h1>
        <ul class="links-list">
<?php dynamic_sidebar('sidebar-5'); ?>            	
        </ul>
    </div><!--end Main Container-->
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
