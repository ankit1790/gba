<?php
/*
Template Name: Data & Mentoring Answers
*/
get_header(); ?>
<div class="intro-banner">
            <div class="container">
                <h3><?php if(is_page('mentoring'))
                   {?>Mentoring Message Board
				   <?php } 
				         if(is_page('data'))
                     {?>Data Message Board
					 <?php } ?></h3>        
            </div>
        </div>
<div class="row row-block">
    <div class="container">
		<div class="row cma-questions-widget cma-main-query">
        <div class="col-md-12">
			<?php 
				if(is_page('mentoring'))
				{
				echo do_shortcode('[cma-questions cat="mentoring-program" form="1" navbar="1" views="1" votes="1" updated="1" answers="1" sortbar="1"]');
				}

				if(is_page('data'))
				{
				echo do_shortcode('[cma-questions cat="data-working-group" form="1" navbar="1" views="1" votes="1" updated="1" answers="1" sortbar="1"]');
				}
			?>
		</div>
		</div>

    </div>
</div>    
<?php get_footer(); ?> 