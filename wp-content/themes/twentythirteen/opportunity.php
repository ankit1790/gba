<?php
/*
  Template Name: The Opportunity
 */
get_header();
?>

<div class="row">
    <div class="full-image-area">
        <div class="full-image"><?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
<?php endif; ?></div>    
        <div class="box-block">
            <div class="banner-text">
                <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php }?>
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {  the_post(); the_title(); }}?></h1>        
            </div>    
        </div>
    </div>
</div>

<!--start slider-->
<div class="row-block heading-block slider-area" id="growth-market"> 
    <!--Start Main Container-->
    <div class="container"> 
        <!--Start row-->
        <div class="row">
            <h1><?php the_field('section_one_head'); ?></h1>
            <span><?php the_field('section_one_description'); ?></span>
            <div>
                <div class="container">
                    <div class="wrapper">
                        <div id="ei-slider" class="ei-slider">
                            <ul class="ei-slider-thumbs">
                                <li class="ei-slider-element">Current</li>
                                <?php
                                query_posts(array('post_type' => 'slider-post', 'categories' => 'growth-market','orderby' => 'date', 'order' => 'ASC','posts_per_page'=>3));
                                //the loop start here
                                $i=1;
                                if (have_posts()) {
                                    while (have_posts()) {
                                        the_post();
                                        if($i==1) $class='class="active"';else $class='';
                                                ?>
                                                <li <?php echo $class?>><a href="#"><span><img src="<?php the_field('inactive_thumbnail'); ?>" alt="thumb01" /><img src="<?php the_field('active_thumbnail'); ?>" alt="thumb01" /></span><?php the_title(); ?></a></li>
                                    <?php $i++;}
                                }
                                wp_reset_query();
                                ?>
                            </ul>
                            <ul class="ei-slider-large">
                                <?php
                                query_posts(array('post_type' => 'slider-post', 'categories' => 'growth-market','orderby' => 'date', 'order' => 'ASC','posts_per_page'=>3));
                                if (have_posts()) {
                                    while (have_posts()) {
                                        the_post();
                                        ?>
                                        <li><img src="<?php echo get_bloginfo('template_directory'); ?>/images/large/1.jpg" alt="large1" />
                                            <div class="row">
                                                <div class="col-md-7 no-padding-left">
                                                    <div class="content-box">
                                                        <?php the_content(); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 float-right no-padding-right">
                                                    <div class="content-box">
        <?php
        $value = get_field('column-2');
        //echo html_entity_decode($value); ?>
       <h2><?php the_field('column-2'); ?></h2>
	   <div class="graph-box"><div class="graph-percent">
	   <p><br><br><br>
	   <?php the_field('description'); ?></p>
	   </div>
	   <div class="graph-chart">
	   <img src="<?php the_field('image'); ?>">
	   </div>
	   </div>
	   
       
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
    <?php }
}
wp_reset_query();
?>
                            </ul>
                        </div>
                    </div><!-- end wrapper --> 
                </div>
            </div>
        </div><!--end row-->
    </div><!--end conatiner-->
</div>
<!--end slider-->

<!--Growth Driver Section-->
<?php query_posts(array('category_name' => 'growth-driver', 'orderby' => 'date', 'order' => 'ASC','posts_per_page'=>3)); ?>
<div id ="growth-drivers" class="row">
    <div class="full-image-area">
        <div class="full-image"><img src="<?php the_field('growth_drivers_image');?>" /></div>
        <div class="box-block">
            <h3><?php the_field('growth_drivers_title');?></h3>
            <ul>
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();?>
                        <li>
                            <h3>
                                <div>                                    
                                    <span class="detail">
                                        <a href="javascript:void(0)"><?php the_title();?></a>
                                        <?php the_content(); ?> 
                                    </span>
                                </div>
                            </h3>
                        </li>
                <?php }
                }
                wp_reset_query();
                ?>            
            </ul>
        </div>
    </div>
</div>
<!--End of Growth Driver Section-->

<div class="row-block heading-block" id="untapped-potential">
    <!--Start Main Container-->
    <div class="container">
        <!--Start row-->
        <div class="row">
            <h1><?php the_field('section_two_head'); ?></h1>
            <span><?php the_field('section_two_description'); ?></span>
            <?php query_posts(array('post_type' => 'untapped_potential','category_name'=>'untapped-potential-opportunity','orderby' => 'date', 'order' => 'ASC','posts_per_page'=>2));
            if (have_posts()) {
                while (have_posts()) {
                    the_post();?>
                    <div class="col-md-6">
                        <div class="content-box">
                            <?php the_title("<h2>","</h2>");?>
                            <strong><?php the_field('post_sub_title');?></strong>
                            <div class="graph-box">
                                <div class="graph-percent">
                                    <h1><?php the_field('percentage_value')?></h1>
                                </div>
                                <div class="graph-chart">
                                    <img src="<?php the_field('percentage_image')?>">
                                </div>
                            </div>
                            <p><?php the_field('graph_info')?></p>
                            <?php the_content();?>
                        </div>
                    </div>
                <?php } }
            wp_reset_query();
            ?>
        </div><!--end row--> 
    </div>	<!--end Main Container-->
</div><!--end row--> 


<!--slider2-->
<div class="row-block heading-block slider-area" id="business-opportunity"> 
    <!--Start Main Container-->
    <div class="container"> 
        <!--Start row-->
        <div class="row">
            <h1><?php the_field('section_three_head'); ?></h1>
            <span><?php the_field('section_three_description'); ?></span>
            <div>
                <div class="container">
                    <div class="wrapper">
                        <div id="ei-slider2" class="ei-slider">
                            <ul class="ei-slider-thumbs">
                                <li class="ei-slider-element">Current</li>
<?php
query_posts(array('post_type' => 'op-slider2','category_name'=>'business-opportunity', 'orderby' => 'date', 'order' => 'ASC','posts_per_page'=>4));
//the loop start here
$i=1;
if (have_posts()) {
    while (have_posts()) {
        the_post();
        if($i==1) $class='class="active"';else $class='';
                                                ?>
                                                <li <?php echo $class?>>
                                    <a href="#"><span><img src="<?php the_field('in-active_thumbnail'); ?>" alt="thumb01" /><img src="<?php the_field('active_thumbnail'); ?>" alt="thumb01" /></span><?php the_title(); ?></a>

                                </li>
    <?php $i++;}
}
wp_reset_query();
?>
                            </ul>
                            <ul class="ei-slider-large">
<?php
query_posts(array('post_type' => 'op-slider2','category_name'=>'business-opportunity', 'orderby' => 'date', 'order' => 'ASC','posts_per_page'=>4));
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
                                        <li><img src="<?php echo get_bloginfo('template_directory'); ?>/images/large/1.jpg" alt="large1" />
                                            <div class="row">
                                                <div class="col-md-7">
                                                    <div class="content-box">
                                                        <?php the_content(); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 float-right">
                                                    <div class="content-box">
                                                        <h2><?php the_field('col2_heading'); ?></h2>
                                                        <div class="graph-box">
                                                            <div class="graph-percent">
                                                                <h1><?php the_field('percentage'); ?></h1>
                                                            </div>
                                                            <div class="graph-chart"><img src="<?php echo the_field('percentage_graph_image'); ?>"/></div>
                                                        </div>
        <?php the_field('col2_description'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
    <?php
    }
}
wp_reset_query();
?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end of slider2-->

<!--Start-row-->
<script type="text/javascript">
    var pieconst = false;
    var charts =  [];
</script>
<div class="row-block result-block piechartblock">
    <!--Start Main Container-->
    <div class="container">
        <h2><?php the_field('result_block_head');?></h2>
        <?php the_field('result_block_description');?>
        <div class="row">
            <?php query_posts(array('post_type' => 'gba-results','category_name'=>'gba-results-opportunity','posts_per_page'=>4,'orderby' => 'date', 'order' => 'ASC'));
if (have_posts()) {
    while (have_posts()) {
        the_post();?>
            <div class="col-md-3">
                <figure><div id="chartContainer<?php echo get_the_ID(); ?>" style="height: 300px; width: 100%;"></div><!--<img src="<?php the_field('result_image');?>"/>--></figure>
                <h3><?php the_field('value');?>%</h3>
                <span><?php the_content();?></span>
            </div>
<script type="text/javascript">
   charts.push('<?php echo get_the_ID(); ?>'+','+'<?php the_field('value');?>'+','+'<?php the_field('source_location');?>');
</script>
<?php }}wp_reset_query();?>
        </div>
    </div><!--end Main Container-->
</div><!--end row-->    



<!--Start-row-->
	<div class="row-block views-block border-block">
            <div class="container">
                <div class="row">
                    <h1><?php the_field('scrolling_quote_head');?></h1>
                    <div id="peer-users" class="owl-carousel">
                        
                            <?php
                            query_posts(array('post_type' => 'peer-learning-users','category_name'=>'scrolling-quote-opportunity'));
                            if (have_posts()) {
                                while (have_posts()) {
                                    the_post();
                                    ?>
                        <div class="item">
                            <?php the_content();
                            the_field('name'); ?>
                            <figure>
                                <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                            </figure>
                            <h5><?php the_title(); ?></h5>
                            <span><?php the_field('sub_title'); ?></span>
                        </div>
                            <?php
                            } }
                        wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div><!--end row--> 
    
<?php
get_footer(); ?> 