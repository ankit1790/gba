<?php
/*
  Template Name: About Us
 */
get_header();
?>
<!--Start row-->    	
<div class="intro-banner">
    <div class="container">
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>
                <h3><?php the_title(); ?></h3>
                <?php
                the_content();
            }
        } wp_reset_query();
        ?>
    </div>
</div>
<!--Start-row-->
<div class="row-block result-block">
    <!--Start Main Container-->
    <div class="container">
        <h2><?php the_field('result_block_head'); ?></h2>
	<span><?php the_field('result_block_description'); ?></span>
        <div class="row">
            <ul><?php
        ?> <?php
                query_posts(array('category_name' => 'gba-results-for-about-us', 'post_type' => 'gba-results', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 4));
                //the loop start here
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <li>
                            <h3> <?php the_field('value'); ?></h3>
                            <span><?php the_content(); ?></span>
                        </li>
                        <?php
                    }
                }
                wp_reset_query();
                ?>         
            </ul>
        </div>
    </div><!--end Main Container-->
</div><!--end row-->
<!--end row--> 
<?php get_footer(); ?>