<?php
/*
Template Name: Library
*/
get_header();
?>
 <div class="intro-banner">
            <div class="container">
                <h3><?php the_title();?></h3>        
            </div>
        </div>
<div class="row row-block">
    <div class="container">
        <div class="col-md-12">
        <?php
            echo do_shortcode('[wpdm-search-page cols="4" items_per_page="16" link_template="5582f78118070"]');
            //echo do_shortcode('[wpdm_packages template="5582f78118070" order_by="date" order="ASC" paging="1" items_per_page="8" title="" desc="" cols=4 colsphone=1 colspad=1]');?>
        </div>
    </div>
</div>            

<?php get_footer(); ?>