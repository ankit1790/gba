<?php get_header(); ?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
<?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
            <?php endif; ?>
            <?php if (!has_post_thumbnail()) : ?>
                <img src="<?php echo get_bloginfo('template_directory'); ?>/images/bank-profile-banner.jpg"/>
            <?php endif; ?>

        </div>
        <div class="box-block">
            <div class="banner-text">
                <h1 class="title"><?php the_title(); ?></h1>        
            </div>    
        </div>
    </div>
</div>
<div class="row-block heading-block">
    <div class="container">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="row">
                <?php
                the_title('<h1>', '</h1>');
                the_content();
                ?>
                </div>
                <!--Start row-->
                <div class="row">
                    <div class="col-md-7">
                        <div class="content-box">
        <?php the_field('left_description') ?>
                        </div>
                    </div>
                    <div class="col-md-4 float-right">
                        <div class="right-block">
        <?php the_field('at_a_glance') ?>
                        </div>
                    </div>
                </div> 
        <?php
    endwhile;
endif;
wp_reset_query();
?>
    </div>
</div>


<!--Start-row-->
<div class="row-block gallery-block">
    <!--Start Main Container-->
    <div class="container">
        <ul class="gallery-list">
            <li>
                <figure>
                    <img src="<?php the_field('small_thumb'); ?>">
                </figure>
            </li>
            <li>
                <figure>
                    <img src="<?php the_field('small_thumb2'); ?>">
                </figure>
            </li>
            <li>
                <figure>
                    <img src="<?php the_field('small_thumb3'); ?>">
                </figure>
            </li>
            <li>
                <figure>
                    <img src="<?php the_field('medium_thumb'); ?>">
                </figure>
            </li>
            <li>
                <figure>
                    <img src="<?php the_field('medium_thumb2'); ?>">
                </figure>
            </li>
            <li>
                <figure>

                    <img src="<?php the_field('small_thumb4'); ?>">
                </figure>
            </li>
            <li>
                <figure>
                    <img src="<?php the_field('small_thumb5'); ?>">
                </figure>
            </li>
            <li>
                <figure>
                    <img src="<?php the_field('small_thumb6'); ?>">
                </figure>
            </li>
            <li>
                <figure>
                    <img src="<?php the_field('small_thumb7'); ?>">
                </figure>
            </li>

        </ul>
    </div><!--end Main Container-->
</div>

<?php get_footer(); ?>