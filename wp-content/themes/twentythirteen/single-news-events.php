<?php
get_header();
?>
<div class="intro-banner">
    <div class="container">
        <h3>News</h3>        
    </div>
</div>
<div class="row-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-box">
                    <?php while ( have_posts() ) : the_post(); 
                    the_title('<h3>','</h3>');
                    the_content();
                    ?>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        <div class="row row-block post-pagination align-center">
            <div class="col-md-12">
                <div class="wp-pagenavi">
                    <?php echo next_post_link('%link', 'Previous', true);
                     echo  previous_post_link('%link', 'Next', true);?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>