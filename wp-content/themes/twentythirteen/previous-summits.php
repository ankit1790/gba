<?php
/*
  Template Name: Previous Summits Page
 */


get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
            <?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
<?php endif; ?>

        </div>
        <div class="box-block">
            <div class="banner-text">
                <!--<span>GBA For Women</span>-->
                <h1 class="title"><?php if (have_posts()) {
    while (have_posts()) {
        the_post();
        the_title();
    }
}
?></h1>        
            </div>    
        </div>


    </div>
</div>
<div class="intro-banner">
    <div class="container">
        <div class="col-md-12">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<!--Start row-->
<div class="row-block heading-block" style="padding-bottom: 0px">



    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <div class="content-box">
<?php echo do_shortcode("[huge_it_slider id='4']"); ?>

                </div>
            </div>

            <div class="col-md-6">
                <div class="content-box">

                    <h1><?php the_field('intro_head'); ?></h1>
                    <span><?php the_field('intro_text'); ?></span>

                </div>
            </div>

        </div>
    </div>
</div>

<!--End row-->
<div class="row-block heading-block">
    <div class="container">     
        <!--Start row-->		
        <div class="row">		
            <?php
            $args = array('post_type' => 'summit', 'category_name' => 'previous-summit', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 2);
            $loop = new WP_Query($args);
            
            //the loop start here

            while ($loop->have_posts()) {
                $loop->the_post();
                ?>
                <div class="col-md-6">
                    <a href="<?php the_permalink(); ?>">
                        <figure class="square-border">
                            <img src='<?php the_field('left_image') ?>'/>
                        </figure>
                    </a>
                    <div class="content-box align-center">
                <?php
                the_title('<h2>', '</h2>');
                the_field('short_description');
                ?>
                        <span class="button-holder"><a class="button" href="<?php the_permalink(); ?>">Read More</a></span>
                    </div>

                </div>
<?php }wp_reset_query(); ?>

        </div> 
        <br>
        <div class="col-md-12">
            <div class="row">

            </div>
        </div>
    </div>
</div>

<div class="row-block views-block border-block">
    <div class="container">
        <div class="row">
            <h1><?php the_field('scrolling_quote_head');?></h1>
            <div id="peer-users" class="owl-carousel">

                    <?php
                    query_posts(array('post_type' => 'peer-learning-users','category_name'=>'scrolling-quote-previous-summit'));
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post();
                            ?>
                        <div class="item">
                        <?php the_content();
                        the_field('name');
                        ?>
                            <figure>
                                <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                            </figure>
                            <h5><?php the_title(); ?></h5>
                            <span><?php the_field('sub_title'); ?></span>
                        </div>
        <?php
    }
}
wp_reset_query();
?>
            </div>
        </div>
    </div>
</div><!--end row--> 





<?php get_footer(); ?>