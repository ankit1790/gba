<?php
/* Template Name: Custom Search */        
get_header(); ?>             
<div class="intro-banner">
    <div class="container">
        <h3>Search Result for : <?php echo "$s"; ?> </h3>        
    </div>
</div>
<div class="row-block news-page heading-block">
    <div class="container">
        <?php 
        $article=0;
        if (have_posts()) : while (have_posts()) : the_post(); 
        $article +=1;
        $wp_query->post_count;
        $wp_query->current_post;
        if ($article % 3 == 1)
            echo '<div class="row">';
        ?>
        <div class="col-md-4 align-center">
            <figure class="news-square-border">
                <?php $image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                if($image)
                { ?>
                <img src="<?php echo $image; ?>"/>
                <?php  } ?>
            </figure>
            <div class="content-box">
                <h3><?php the_title(); ?></h3>
                <p><?php the_field('short_description'); ?></p>
                <?php if(get_field('other_link')){?>
                <span><a target="_blank" href="<?php the_field('other_link');?>" class="button">Read More</a></span>
                <?php }else{?>
                <span><a target="_blank" href="<?php the_permalink();?>" class="button">Read More</a></span>
                <?php }?>
            </div>
        </div>
        <?php if($article % 3 == 0 || $wp_query->current_post+1 == $wp_query->post_count) echo '</div>';?>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>