<?php
/*
Template Name: Vault
*/
get_header();
$obj = new Sidebar_Login_Widget();
?>
<?php if(is_user_logged_in()){?>
 <div class="intro-banner">
            <div class="container">
                <h3><?php the_title();?></h3>        
            </div>
        </div>
<?php }?>
<div class="row row-block">
    <div class="container">
        <div class="row text-center">
           <div class="col-md-6 column-centered">
                <?php if(!is_user_logged_in()){?>
                <h1>LOGIN TO THE VAULT</h1>
                <?php echo do_shortcode('[wppb-login lostpassword_url="'.get_permalink( 920).'"]');
                }?>
           </div>
           
        </div>
        <div class="col-md-12 package-list">
                <?php  if(is_user_logged_in()){
                    if(have_posts()) {
                     while (have_posts()) : 
                        the_post(); 
                        the_content(); 
                     endwhile;
                    }

                }
                ?>
            </div>
        <div class="col-md-12">
            
        </div>
    </div>
</div>
<?php get_footer(); ?>

