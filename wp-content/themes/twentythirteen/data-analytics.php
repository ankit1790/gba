<?php
/*
  Template Name: Data Analytics
 */
get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
            <?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
            <?php endif; ?>
        </div>    
        <div class="box-block">
            <div class="banner-text">
		<?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
		<?php }?>
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {  the_post(); ?><?php the_title();}} ?>  </h1>        
            </div>    
        </div>
    </div>
</div>

<!--start slider-->
<div class="row-block heading-block" style="padding-bottom:0"> 
    <!--Start Main Container-->
    <div class="container" id="growth-market"> 
        <!--Start row-->
        <div class="row">
            <h1><?php the_field('head_first');?></h1>
            <?php if (have_posts()) {
                while (have_posts()) {  the_post(); ?>
                <span><?php the_content(); ?></span>
            <?php  }}?>
        </div>
    </div><!--end row-->
</div><!--end conatiner-->
<!--end slider-->

<div class="row-block heading-block padding-top-0">
    <!--Start Main Container-->
    <div class="container" id="untapped-potential">
        <!--Start row-->
        <div class="row">
            <h1><?php the_field('head_second');?></h1>
            <span><?php the_field('second_head_content');?></span>
            <?php query_posts(array('post_type' => 'untapped_potential','category_name'=>'untapped-potential-data-analytics','orderby' => 'date', 'order' => 'ASC','posts_per_page'=>2));
            if (have_posts()) {
                while (have_posts()) {
                    the_post();?>
                    <div class="col-md-6">
                        <div class="content-box">
                            <?php the_title("<h2>","</h2>");?>
                             <strong><?php the_field('post_sub_title');?></strong>
                            <div class="graph-box">
                                <div class="graph-percent">
                                    <h1><?php the_field('percentage_value')?></h1>
                                </div>
                                <div class="graph-chart">
                                    <img src="<?php the_field('percentage_image')?>">
                                </div>
                            </div>
                            <p><?php the_field('graph_info')?></p>
                            <?php the_content();?>
                        </div>
                    </div>
                <?php } }
            wp_reset_query();
            ?>
        </div><!--end row--> 
    </div>	<!--end Main Container-->
</div><!--end row--> 

<script type="text/javascript">
    var pieconst = false;
    var charts =  [];
</script>
<!--Start-row-->
<div class="row-block result-block piechartblock">
    <!--Start Main Container-->
    <div class="container">
        <h2><?php the_field('result_block_head');?></h2>
        <?php the_field('result_block_description');?>
        <div class="row">
            <?php query_posts(array('post_type' => 'gba-results','category_name'=>'gba-results-data-analytics','posts_per_page'=>4,'orderby' => 'date', 'order' => 'ASC'));
if (have_posts()) {
    while (have_posts()) {
        the_post();?>
            <div class="col-md-3">
                <figure><div id="chartContainer<?php echo get_the_ID(); ?>" style="height: 300px; width: 100%;"></div><!--<img src="<?php the_field('result_image');?>"/>--></figure>
                <h3><?php the_field('value');?>%</h3>
                <span><?php the_content();?></span>
            </div>
<script type="text/javascript">
   charts.push('<?php echo get_the_ID(); ?>'+','+'<?php the_field('value');?>'+','+'<?php the_field('source_location');?>');
</script>
<?php }}wp_reset_query();?>
        </div>
    </div><!--end Main Container-->
</div><!--end row-->    



<?php get_footer(); ?> 