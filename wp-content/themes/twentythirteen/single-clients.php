<?php

get_header(); ?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
           <?php if (has_post_thumbnail()) : ?>
            <?php the_post_thumbnail('full'); ?>
            <?php endif; ?>
            <?php if (!has_post_thumbnail()) : ?>
               <img src="<?php echo get_bloginfo('template_directory'); ?>/images/bank-profile-banner.jpg"/>
<?php endif; ?>
            
        </div>
            <div class="box-block">
            <div class="banner-text">
            <?php 
                   if( get_field('banner_first_head') ):  ?>
                <span> <?php the_field('banner_first_head') ?></span>
                   <?php endif; ?>
                <h1 class="title"><?php the_title();?></h1>        
            </div>    
        </div>
    </div>
</div>
<div class="row-block heading-block">
    <div class="container">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();?>
        <div class="row">
            <?php
                    the_title('<h1>','</h1>');
                    the_content();
            ?>
        </div>
        <!--Start row-->
        <div class="row">
            <div class="col-md-7">
                <div class="content-box">
                    <?php the_field('left_description') ?>
                </div>
            </div>
            <div class="col-md-4 float-right">
                <div class="right-block">
                    <?php the_field('at_a_glance') ?>
                </div>
            </div>
        </div> 
        <?php 
        endwhile;
        endif; 
        wp_reset_query();
        ?>
    </div>
</div>


 <!--Start-row-->
	<div class="row-block gallery-block">
    <!--Start Main Container-->
		<div class="container">
<?php if (get_field('video') && get_field('video') != "") { ?>
<iframe  width="800" height="420" src="<?php echo the_field('video'); ?>" frameborder="0" allowfullscreen></iframe>
<?php } else{ ?><?php 
                   if( get_field('small_image') ):  ?>
        	<ul class="gallery-list">
            	<li>
                <figure>
            	  <img src="<?php the_field('small_image'); ?>">
                 </figure>
                </li>
            	<li>
                 <figure>
            	 <img src="<?php the_field('small_image2'); ?>">
                 </figure>
            	</li>
            	<li>
                <figure>
            	 <img src="<?php the_field('small_image3'); ?>">
                 </figure>
            	</li>
            	<li>
                <figure>
            	  <img src="<?php the_field('medium_image'); ?>">
                 </figure>
            	</li>
                 <li>
                <figure>
            	<img src="<?php the_field('medium_image2'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>

            	 <img src="<?php the_field('small_image4'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	 <img src="<?php the_field('small_image5'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	  <img src="<?php the_field('small_image6'); ?>">
                 </figure>
            	</li>
                <li>
                <figure>
            	 <img src="<?php the_field('small_image7'); ?>">
                 </figure>
            	</li>

                </ul> <?php endif; ?>
<?php } ?>
        	</div><!--end Main Container-->
        </div>

<div class="row-block views-block border-block">
    <div class="container">
        <div class="row">
            <h1><?php the_field('scrolling_quote_head');?></h1>
            <div id="peer-users" class="owl-carousel">

                    <?php
                    query_posts(array('post_type' => 'peer-learning-users','category_name'=>'scrolling-quote-clients'));
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post();
                            ?>
                <div class="item">
                    <?php the_content();
                    the_field('name'); ?>
                    <figure>
                        <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                    </figure>
                    <h5><?php the_title(); ?></h5>
                    <span><?php the_field('sub_title'); ?></span>
                </div>
                    <?php
                    } }
                wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div><!--end row--> 


<?php get_footer(); ?>