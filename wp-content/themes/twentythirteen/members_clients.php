<?php
/*
Template Name: Members&Clients
*/
get_header();?>
<!--Start row-->    	
<?php if (have_posts()) {
    while (have_posts()) {  the_post();
    $pageName =  get_query_var('pagename');
    if($pageName=='my-members'){ 
        $title = 'Members';?>
        <div class="intro-banner">
            <div class="container">

                <h3><?php the_title(); ?></h3>        
            </div>
        </div>
    <?php }else 
        {
        $title = the_title('','',false);?>
        <div class="row">
            <div class="full-image-area">
                <div class="full-image"><?php if (has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail('full'); ?>
                    <?php endif; ?>
                </div>
          <div class="box-block">
            <div class="banner-text">
               <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php } if($pageName!='speakers'){?>
                <h1 class="title"><?php the_title();?></h1>
		<?php }?>        
            </div>    
        </div>
            </div>
        </div>
    <?php }?>
        <!--end row--> 
  <?php  if($pageName=='speakers'){ ?>		
<div class="row intro-banner">
    <div class="container">
        <div class="col-md-12">
            <ul class="greenbar-menu">
                <li><a href="<?php echo get_permalink(1056); ?>">About</a></li> 
                <li><a href="<?php echo get_permalink(1058); ?>">Agenda</a></li>
                <li><a href="<?php echo get_permalink(1060); ?>">Speakers</a></li>
                <li><a href="<?php echo get_permalink(1062); ?>">Sponsors</a></li>
                <li><a href="<?php echo get_permalink(1064); ?>">Location</a></li>
                <li><a href="<?php echo get_permalink(1066); ?>">Register</a></li>
                <li><a href="<?php echo get_permalink(1068); ?>">Previous Summits</a></li>
            </ul>
        </div>
    </div>
</div>	
<?php } ?>		
        <div class="row-block heading-block" style="padding-bottom: 0px">
            
 
            <h1><?php 
            if(get_field('intro_head'))
                the_field('intro_head');
            else if(get_field('intro_title'))
		the_field('intro_title');
	    else
                echo 'Intro';
            ?></h1>
            <div class="container">
                <div class="row">
                    <span><?php 
                    if(get_field('intro_text'))
                        the_field ('intro_text');
                    else
                        the_field('short_description');?></span>
                </div>
            </div>
        </div>
        
<?php }}
wp_reset_query();?>
        <!-- Start Map -->

  <?php  if($pageName=='my-members'){ ?>
  <div class="row row-block summit-slider">
     <div class="container">
	<div class="col-md-12 column-centered align-center">
     <?php //dynamic_sidebar('home_page_banner_first'); ?>
     <?php  the_field('map_above_description') ?>
       </div>           
 <?php
   
echo do_shortcode('[show-map id="1"]');?>
         </div>
</div>
<?php }?> 
      
   
<!-- End Map -->
<div class="container">
<div class="col-md-12 column-centered align-center">
<?php  the_field('map_below_description') ?>
</div>
</div>
<div class="row-block heading-block">
    <!--Start Main Container-->
    <div class="container">
            <?php
            if($pageName=='my-members'){
                $posttype = 'members';
                $link = 'member-profiles';
            }
            else if($pageName=='impact'){
                $posttype = 'clients';
                $link = 'client-profile';
            }
			else if($pageName=='speakers'){
                $posttype = 'Speaker';
                $link = 'client-profile';
            }
            
            $categoryName = 'leadership';
            if ( get_query_var('paged') ) {
            $paged = get_query_var('paged');
            } else if ( get_query_var('page') ) {
                $paged = get_query_var('page');
            } else {
                $paged = 1;
            }
            $args = array('post_type'=>$posttype,'orderby' => 'date', 'order' => 'DESC','posts_per_page'=>300);
            $loop = new WP_Query( $args );
            //the loop start here
            $i=0;
            while ( $loop->have_posts() ) { $loop->the_post();
            $i++;if($i%3==1) echo '<div class="row">';
            ?>
                    <div class="col-md-4">
                        <figure class="square-border"><img src="<?php echo the_field('avatar'); ?>"/></figure>
                        <div class="content-box align-center">
                            <h2><?php echo the_title(); ?></h2>
                            <?php $str = get_the_content(); 
                            echo "<p>".substr($str, 0,300)."</p>"; ?>
                            <span><a href="<?php the_permalink();?>" class="button">Read More</a></span>
                        </div>
                    </div>
           <?php
           if($i%3==0 || $loop->current_post+1 == count($loop->posts)) echo '</div>';
            }
        wp_reset_postdata();?>
    </div>	<!--end Main Container-->
</div>
<!--end row-->
 <?php  if (have_posts()) {
    while (have_posts()) {  the_post();
    $pageName =  get_query_var('pagename');
    if($pageName=='my-members') 
        { ?>
		<!--Start-row-->
<div class="row-block views-block border-block">
    <div class="container">
        <div class="row">
            <h1><?php the_field('scrolling_quote_head') ?></h1>
            <div id="peer-users" class="owl-carousel">

                    <?php
                    query_posts(array('post_type' => 'peer-learning-users','category_name'=>'scrolling-quote-members'));
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post();
                            ?>
                <div class="item">
                    <?php the_content();
                    the_field('name'); ?>
                    <figure>
                        <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                    </figure>
                    <h5><?php the_title(); ?></h5>
                    <span><?php the_field('sub_title'); ?></span>
                </div>
                    <?php
                    } }
                wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div><!--end row--> 
		
		
		<?php } else {?>

 <?php } }} ?>
 <?php get_footer(); ?>