<?php
/*
Template Name: Peer Learning
*/
get_header(); ?>
<div class="row">
  <div class="full-image-area">
    <div class="full-image"><?php if ( has_post_thumbnail() ) : ?>
		<?php the_post_thumbnail( 'full' ); ?>
	<?php endif; ?></div>    
    <div class="box-block">
    <div class="banner-text">
        <?php if(get_field('banner_first_head')){?>
        <span><?php the_field('banner_first_head'); ?></span>
        <?php }?>
        <h1 class="title"><?php the_title();?></h1>        
    </div>    
    </div>
  </div>
</div>

<div class="row-block heading-block">
    <!--Start Main Container-->
    <div class="container">
        <!--Start row-->
        <div class="row"><?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                    <span><?php the_content();?></span>
                    <?php endwhile; else: ?>
                    <?php endif; ?>
        
        </div><!--end row-->
        <div class="row">
            
            <?php $children = get_pages( 
    array(
        'sort_column' => 'menu_order',
        'sort_order' => 'ASC',
        'hierarchical' => 0,
        'parent' => $post->ID,
        'post_type' => 'page',
    ));

foreach( $children as $post ) { 
        setup_postdata( $post ); ?>
            <div class="col-md-4 align-center">
                    <figure class="circle-border"><img src="<?php the_field('image_icon'); ?>"/></figure>
                    <div class="content-box">
                        <h2><?php the_title(); ?></h2>
                        <p><?php the_field('short_description'); ?></p>
                        <span><a class="button" href="<?php echo get_permalink(); ?>">Read More</a></span>
                    </div>      
            </div>
            <?php } wp_reset_query();?>
        </div>
    </div>	<!--end Main Container-->
</div><!--end row--> 

    <!--Start-row-->
	<div class="row-block views-block border-block">
            <div class="container">
                <div class="row">
                    <h1><?php the_field('scrolling_quote_head');?></h1>
                    <div id="peer-users" class="owl-carousel">
                        
                            <?php
                            query_posts(array('post_type' => 'peer-learning-users','category_name'=>'scrolling-quote-peer-learning'));
                            if (have_posts()) {
                                while (have_posts()) {
                                    the_post();
                                    ?>
                        <div class="item">
                            <?php the_content();
                            the_field('name'); ?>
                            <figure>
                                <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                            </figure>
                            <h5><?php the_title(); ?></h5>
                            <span><?php the_field('sub_title'); ?></span>
                        </div>
                            <?php
                            } }
                        wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div><!--end row--> 
    
<?php get_footer(); ?> 