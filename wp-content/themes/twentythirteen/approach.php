<?php
/*
  Template Name: The Approach
 */
get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image"><?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?><?php endif; ?></div>    
        <div class="box-block">
            <div class="banner-text">
                <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php }?>
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {  the_post(); the_title(); }}?></h1>        
            </div>    
        </div>
    </div>
</div>
<div class="row-block heading-block" id="customer-insights">
    <!--Start Main Container-->
    <div class="container">
        <!--Start row-->
        <div class="row">
            <h1><?php the_field('section_one_head'); ?></h1>
            <span><?php the_field('section_one_description'); ?></span>
            <div class="col-md-6">
                
                <div class="content-box">
<?php the_field('left_top'); ?>
                    <div class="slider-wrapper">
                        <div id="cust-insights-left" class="owl-carousel">
                        <?php
                                query_posts(array('post_type' => 'cust_insight_posts','category_name'=>'customer-insight-left-posts'));
                                if (have_posts()) {
                                    while (have_posts()) {
                                        the_post();
                                        ?>
                                        <div class="item">
                                            <div class="graph-box">
                                                <div class="graph-percent"><h1><?php the_field('value')?>%</h1></div>
                                                <div class="graph-chart"><?php the_content();?></div>
                                            </div>
                                        </div>
                                <?php
                                } }
                            wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
                <div class="content-box">
<?php the_field('left_bottom'); ?>
                </div>
            </div><!--col-md-7-->

            <div class="col-md-6">
                <div class="content-box">
<?php the_field('right_top'); ?>
                </div>
                <div class="content-box">
<?php the_field('right_bottom'); ?>
                    <div class="slider-wrapper">
                        <div id="cust-insights-right" class="owl-carousel">
                        <?php
                                query_posts(array('post_type' => 'cust_insight_posts','category_name'=>'customer-insight-right-posts'));
                                if (have_posts()) {
                                    while (have_posts()) {
                                        the_post();
                                        ?>
                                        <div class="item">
                                            <div class="graph-box">
                                                <div class="graph-percent"><h1><?php the_field('value')?>%</h1></div>
                                                <div class="graph-chart"><?php the_content();?></div>
                                            </div>
                                        </div>
                                <?php
                                } }
                            wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div><!--end-col-md-4-->
        </div><!--end row-->
    </div>
</div><!--end row--> 


<div class="row-block heading-block border-block" id="value-creation">
    <h1><?php the_field('section_two_head'); ?></h1>
    <span><?php the_field('section_two_description'); ?></span>
    <div class="full-image-area">
        <div class="full-image">
            <?php if(get_field('section_two_background')){
                echo '<img src="'.  get_field('section_two_background').'" />';
            }else{
                echo '<img src="'.get_bloginfo('template_directory').'/images/value-creation.jpg" />';
            }
            ?>
        </div>
        <?php query_posts(array('category_name' => 'value-creation', 'orderby' => 'date', 'order' => 'ASC', 'posts_per_page' => 4)); ?>
        <div class="box-block">
            <ul class="no-heading">
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <li>
                            <h3>
                                <div>                       
                                    <span class="detail"><a href="javascript:void(0)" ><?php the_title(); ?></a><?php the_content(); ?> </span>
                                </div>
                            </h3>
                        </li>
                    <?php
                    }
                }
                wp_reset_query();
                ?>
            </ul>
        </div>
    </div>    
</div>

<div class="row-block" id="segmentation">
    <!--Start Main Container-->
    <div class="container heading-block slider-area">
        <!--Start row-->
        <div class="row">
            <h1><?php the_field('section_three_head'); ?></h1>
            <span><?php the_field('section_three_description'); ?></span>
            <div class="col-md-6">
                <div class="content-box">
                    <h2><?php the_field('section_three_left_head'); ?></h2>
                    <p><?php the_field('section_three_left_description'); ?></p>
                    <!--slider section -->
                    <div>
                        <div class="container">
                            <div class="wrapper">
                                <div id="ei-slider3" class="ei-slider">
                                    <ul class="ei-slider-thumbs">
                                        <li class="ei-slider-element">Current</li>
                                        <?php
                                        query_posts(array('post_type' => 'segmentation-post', 'category_name' => 'business-owners', 'orderby' => 'date', 'order' => 'ASC', 'posts_per_page' => 3));
                                        //the loop start here
                                        $i=1;
                                        if (have_posts()) {
                                            while (have_posts()) {
                                                the_post();
                                                if($i==1) $class='class="active"';else $class='';
                                                ?>
                                                <li <?php echo $class?>>
                                                    <a href="#"><span><img src="<?php the_field('inactive_thumbnail'); ?>" alt="thumb01" /><img src="<?php the_field('active_thumbnail'); ?>" alt="thumb01" /></span><?php the_title(); ?></a>
                                                </li>
                                            <?php $i++;}
                                        }wp_reset_query();
                                        ?>
                                    </ul>
                                    <ul class="ei-slider-large">
                                        <?php
                                        query_posts(array('post_type' => 'segmentation-post', 'category_name' => 'business-owners', 'orderby' => 'date', 'order' => 'ASC', 'posts_per_page' => 3));
                                        //the loop start here
                                        if (have_posts()) {
                                            while (have_posts()) {
                                                the_post();
                                                ?>
                                                <li><img src="<?php echo get_bloginfo('template_directory'); ?>/images/large/1.jpg" alt="large1" />
        <?php the_content(); ?>
                                                </li>
    <?php }
}wp_reset_query();
?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end of slider section-->
                </div>      
            </div><!--col-md-7-->
            <div class="col-md-6">
                <div class="content-box">
                    <h2><?php the_field('section_three_right_head'); ?></h2>
                    <p><?php the_field('section_three_right_description'); ?></p>
                    <div>
                        <div class="container">
                            <div class="wrapper">
                                <div id="ei-slider3" class="ei-slider">
                                    <ul class="ei-slider-thumbs">
                                        <li class="ei-slider-element">Current</li>
                                        <?php
                                        query_posts(array('post_type' => 'segmentation-post', 'category_name' => 'individual-consumers', 'orderby' => 'date', 'order' => 'ASC'));
                                        //the loop start here
                                        $i=1;
                                        if (have_posts()) {
                                            while (have_posts()) {
                                                the_post();
                                                if($i==1) $class='class="active"';else $class='';
                                                ?>
                                                <li <?php echo $class?>>
                                                    <a href="#"><span><img src="<?php the_field('inactive_thumbnail'); ?>" alt="thumb01" /><img src="<?php the_field('active_thumbnail'); ?>" alt="thumb01" /></span><?php the_title(); ?></a>
                                                </li>
    <?php $i++; }
}wp_reset_query();
?>
                                    </ul>
                                    <ul class="ei-slider-large">
<?php
query_posts(array('post_type' => 'segmentation-post', 'category_name' => 'individual-consumers', 'orderby' => 'date', 'order' => 'ASC'));
//the loop start here
if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
                                                <li><img src="<?php echo get_bloginfo('template_directory'); ?>/images/large/1.jpg" alt="large1" />
        <?php the_content(); ?>
                                                </li>
    <?php }
}wp_reset_query();
?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end of slider section-->
                </div>      
            </div><!--end-col-md-4-->
        </div><!--end row--> 
    </div>	<!--end Main Container-->
</div><!--end row--><!--Start-row--><!--end row-->
<style type="text/css">
    .owl-theme .graph-chart > p {
        margin: 35px 0 0;
    }
</style>

<!--Start-row--><!--end row--> 

<!--Start-row-->
	<div class="row-block views-block border-block">
            <div class="container">
                <div class="row">
                    <h1><?php the_field('scrolling_quote_head');?></h1>
                    <div id="peer-users" class="owl-carousel">
                        
                            <?php
                            query_posts(array('post_type' => 'peer-learning-users','category_name'=>'scrolling-quote-approach'));
                            if (have_posts()) {
                                while (have_posts()) {
                                    the_post();
                                    ?>
                        <div class="item">
                            <?php the_content();
                            the_field('name'); ?>
                            <figure>
                                <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                            </figure>
                            <h5><?php the_title(); ?></h5>
                            <span><?php the_field('sub_title'); ?></span>
                        </div>
                            <?php
                            } }
                        wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div><!--end row--> 


<?php get_footer(); ?> 