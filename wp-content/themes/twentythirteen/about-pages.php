<?php
/*
Template Name: About Pages
*/
get_header(); ?>
<!--Start row-->    	
<?php if (have_posts()) {
    while (have_posts()) {  the_post();?>
        <div class="intro-banner">
            <div class="container">
                <h3><?php the_title();?></h3>        
            </div>
        </div>
        <!--end row--> 
        <div class="row-block heading-block" style="padding-bottom: 0px">
            <h1><?php the_field('intro_title') ?></h1>
            <div class="container">
                <div class="row">
                    <span><?php the_content();?></span>
                </div>
            </div>
        </div>
<?php }}?>
<div class="heading-block">
    <!--Start Main Container-->
    <div class="container">
        <div class="row">
            <?php
            $pageNmae =  get_query_var('pagename');
            if($pageNmae=='leadership') $categoryName = 'leadership';
            else if($pageNmae=='gba-team') $categoryName = 'gba_team';
            else if($pageNmae=='sponsors') $categoryName = 'sponsors';
            query_posts(array('category_name'=>$categoryName,'orderby' => 'date', 'order' => 'DESC','posts_per_page'=>24));
            //the loop start here
            if (have_posts()) {
                while (have_posts()) {  the_post(); ?>
                    <div class="col-md-4">
                        <figure class="square-border"><?php echo the_post_thumbnail('full'); ?></figure>
                        <div class="content-box align-center">
                            <h2><?php echo the_title(); ?></h2>
                            <?php //$str = get_the_content(); 
                            //echo "<p>".substr($str, 0,300)."</p>"; ?>
                            <p><?php the_field('short_description') ?></p>
                            <span><button onclick="window.location='<?php the_permalink();?>'" type="button">Learn More</button></span>
                        </div>
                    </div>
            <?php } }
                wp_reset_query();
            ?>
        </div>
    </div>	<!--end Main Container-->
</div><!--end row--> 

<?php get_footer(); ?>