<?php
/*
Template Name: Mentoring & Study Tours
*/
get_header(); ?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image">
            <?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
            <?php endif; ?>
        </div>
         <div class="box-block">
            <div class="banner-text">
                <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php }?>
                <h1 class="title"><?php if (have_posts()) {
                while (have_posts()) {  the_post(); ?><?php the_title(); ?>
                <?php  }}?>
                </h1>        
            </div>    
        </div>
    </div>
</div>
<div class="row-block">
    <div class="col-md-12">
        <div class="container">
            <?php if (have_posts()) {
                    while (have_posts()) {  the_post(); ?>
                    <div class="row heading-block">
                        <h1><?php the_field('intro_head');?></h1>
                        <span><?php the_field('intro_text');?></span>
                    </div>
                    <div class="row">
                    <?php the_content(); ?>
                    <figure class="text-center"><img src="<?php the_field('photo');?>"/></figure>
                    </div>
            <?php  }}?>
        </div>
    </div>
</div>
<div class="row-block views-block border-block">
            <div class="container">
                <div class="row">
                    <h1><?php the_field('scrolling_quote_head');?></h1>
                    <div id="peer-users" class="owl-carousel">
                        
                            <?php
                            $pageName =  get_query_var('pagename');
                            if($pageName=='mentoring-program') $category = 'scrolling-quote-mentoring';
                            else $category = 'scrolling-quote-study-tours';
                            query_posts(array('post_type' => 'peer-learning-users','category_name' => $category));
                            if (have_posts()) {
                                while (have_posts()) {
                                    the_post();
                                    ?>
                        <div class="item">
                            <?php the_content();
                            the_field('name'); ?>
                            <figure>
                                <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                            </figure>
                            <h5><?php the_title(); ?></h5>
                            <span><?php the_field('sub_title'); ?></span>
                        </div>
                            <?php
                            } }
                        wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div>
<?php get_footer(); ?>
