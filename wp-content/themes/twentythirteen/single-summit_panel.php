<?php
/**
 * The template for displaying all single summit pannel type posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="intro-banner">
    <div class="container">
        <h3><?php the_title(); ?></h3>        
    </div>
</div>
<div class="row-block heading-block">
    <div class="container">
        
        <div class="row">
           
        </div>
        <!--Start row-->
        <div class="row">
            <div class="col-md-7">
                <div class="content-box">
                 <?php
        if (have_posts()) : while (have_posts()) : the_post();?>  <?php
                  
                    the_content();
            ?>
                </div>
            </div>
            <div class="col-md-4 float-right">
                <div class="right-block">
                 <?php the_post_thumbnail('full'); 
                 if(get_field('panel_list_names')){
                     the_field('panel_list_names');
                 }
                 ?>
                </div>
            </div>
        </div> 
        <?php 
        endwhile;
        endif; 
        wp_reset_query();
        ?>
    </div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>