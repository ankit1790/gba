<?php
/*
  Template Name: Summit
 */
get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image"><?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
<?php endif; ?></div>    
        <div class="box-block">
            <div class="banner-text">
                <?php if(get_field('banner_first_head')){?>
                <span><?php the_field('banner_first_head'); ?></span>
                <?php }?>
                <h1 class="title"><?php //the_title(); ?></h1>        
            </div>    
        </div>
    </div>
</div>

<div class="row intro-banner">
    <div class="container">
        <div class="col-md-12">
            <ul class="greenbar-menu">
                <li><a href="<?php echo get_permalink(1056); ?>">About</a></li> 
                <li><a href="<?php echo get_permalink(1058); ?>">Agenda</a></li>
                <li><a href="<?php echo get_permalink(1060); ?>">Speakers</a></li>
                <li><a href="<?php echo get_permalink(1062); ?>">Sponsors</a></li>
                <li><a href="<?php echo get_permalink(1064); ?>">Location</a></li>
                <li><a href="<?php echo get_permalink(1066); ?>">Register</a></li>
                <li><a href="<?php echo get_permalink(1068); ?>">Previous Summits</a></li>
            </ul>
        </div>
    </div>
</div>	
<div class="row align-center">
    <div class="full-image">
        <img src="<?php the_field('photo'); ?>"/>
    </div>
</div>
<div class="row-block heading-block video-block">
    <!--Start Main Container-->
    <div class="container">
        <!--Start row-->
        <div class="row">
            <ul class="incorporate-icons">

                <?php
                query_posts(array('post_type' => 'summit', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 4));
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <li><a href="<?php the_permalink(); ?>"> <?php //the_post_thumbnail('full'); ?></a></li>

                        <?php
                    }
                }
                wp_reset_query();
                ?>    

            </ul>

            <h1><?php the_field('intro_head');?></h1>
            <span><?php the_field('intro_text');?></span>
        </div><!--end row-->

        <!--Start row-->
        <div class="row">
            <div class="col-md-7" id="video-container">
                <?php
		the_field('what_we_do_video');
                ?>
            </div><!--col-md-7-->

            <div class="col-md-5 video-content">
                <div class="content-box">
                    <!--<h2><?php echo get_the_title($page_data->ID); ?></h2>
<?php echo strip_shortcodes(get_post_field('post_content', $page_data->ID)); ?>-->
		<?php the_field('what_we_do_video_inside_text'); ?>
                </div>
            </div><!--end-col-md-4-->
        </div><!--end row--> 
    </div>	<!--end Main Container-->
</div><!--end row--> 

<!--Start-row-->
<div class="row row-block summit-slider">
    <div class="col-md-12">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Deleted Building solutions together -->    
                    <h1>Latest News</h1>
                    <?php
                    $args = array('post_type' => 'news-events', 'category_name' => 'gba_summit_news', 'orderby' => 'date', 'order' => 'DESC');
                    $loop = new WP_Query($args);
                    //echo "<pre>";print_r($loop);die;
                    //the loop start here

                    while ($loop->have_posts()) {
                        $loop->the_post();
                        ?>
                        <div class="col-md-4 align-center">
                            <figure class="square-border">
                                <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>"/>
                            </figure>
                            <div class="content-box">
                                <h2><?php the_title(); ?></h2>
                                <p><?php the_field('short_description'); ?></p>
                                <span class="button-holder"><a href="<?php the_permalink();?>" class="button">Read More</a></span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end row-->	

<div class="row row-block align-center">
    <div >
        <?php echo do_shortcode("[huge_it_slider id='3']"); ?>
    </div>
</div>
<!--Start-row-->
<div class="row-block views-block border-block">
    <div class="container">
        <div class="row">
            <h1><?php the_field('scrolling_quote_head');?></h1>
            <div id="peer-users" class="owl-carousel">

                <?php
                query_posts(array('post_type' => 'peer-learning-users','category_name'=>'scrolling-quote-summit'));
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <div class="item">
        <?php the_content();
        the_field('name');
        ?>
                            <figure>
                                <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" class="img-circle">
                            </figure>
                            <h5><?php the_title(); ?></h5>
                            <span><?php the_field('sub_title'); ?></span>
                        </div>
        <?php
    }
}
wp_reset_query();
?>
            </div>
        </div>
    </div>
</div><!--end row--> 



<!--Start-row-->
<div class="row-block twitter-block border-block">
  <div class="container">
    <!--Start Main Container-->
        <?php dynamic_sidebar('summit-tweets'); ?>
   </div> 
</div><!--end row-->


<!--Start-row-->
<div class="row-block links-block">
    <!--Start Main Container-->
    <div class="container">
        <h1>Our Sponsors</h1>
	<div id="sponsors-slider" class="owl-carousel" style="padding: 0 35px">
                <?php dynamic_sidebar('summit_sponsors'); ?>            	
        </div>
    </div><!--end Main Container-->
</div>



<!--Start-row--><!--Start-row-->
<style type="text/css">
    #sponsors-slider .owl-prev {
        left: 0;
        position: absolute;
        top: 55px;
    }
    #sponsors-slider .owl-next {
        position: absolute;
        right: 0;
        top: 55px;
    }
    #sponsors-slider .owl-item img{width: auto;display: inline-block}
</style>
<?php get_footer(); ?> 