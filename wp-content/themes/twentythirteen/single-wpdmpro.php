<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
                    <div class="container package-detail">
                        <div class="row w3eden">
                            <div class="col-md-8 margin-bottom-10">
                                <?php /* The loop */ ?>
                                <?php while ( have_posts() ) : the_post(); ?>

                                    <?php get_template_part( 'package-content', get_post_format() ); ?>
                                    <?php comments_template(); ?>
                                <?php endwhile; ?>
                            </div>
                            <div class="col-md-4 margin-bottom-10">
                                <div class="sidebar">
                                    <?php dynamic_sidebar('package-share-sidebar'); ?>
                                </div>
                                <div class="thumbnail navg">
                                    <div class="col-md-6">
                                        <?php $prev_post_id = get_previous_post()->ID;?>
                                        <a class="btn btn-info btn-block btn-lg" href="<?php echo get_permalink( $prev_post_id ); ?>">
                                            <i class="fa fa-angle-left"></i> Previous
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <?php $next_post_id = get_next_post()->ID; ?>
                                        <a class="btn btn-warning btn-block btn-lg" href="<?php echo get_permalink( $next_post_id ); ?>">
                                            Next <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                    <?php //twentythirteen_post_nav(); ?>
                                </div>
                                <?php 
                                $posttags = get_the_tags(get_the_ID());?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">Tags</div>
                                    <div class="panel-body">
                                        <ul>
                                        <?php if ($posttags) {
                                            foreach($posttags as $tag) {
                                                    echo '<li><a rel="tag" href="'.get_tag_link($tag->term_id).'">'.$tag->name .'</a></li>'; 
                                            }
                                        }else{
                                            echo '<li></li>';
                                        }?>
                                        </ul>
                                    </div>
                                </div>
                                <?php 
                                //dynamic_sidebar('package-tag-sidebar'); 
                                dynamic_sidebar('new-downloads-sidebar'); 
                                dynamic_sidebar('top-downloads-sidebar'); 
                                ?>
                            </div>
                        </div>
                    </div>
		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>