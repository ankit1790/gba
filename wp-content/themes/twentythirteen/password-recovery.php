<?php
/*
  Template Name: Password Recovery
 */
get_header();
?>
<div class="row row-block">
    <div class="container">
        <div class="row text-center">
        <div class="col-md-7 column-centered">
            <?php echo do_shortcode('[wppb-recover-password]');?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>