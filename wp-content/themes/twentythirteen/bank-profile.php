<?php
/*
  Template Name: Bank Profile
 */
get_header();
?>
<div class="row">
    <div class="full-image-area">
        <div class="full-image"><?php if (has_post_thumbnail()) : ?>
                <?php the_post_thumbnail('full'); ?>
            <?php endif; ?></div>    
    </div>
</div>

<div class="row-block heading-block">
    <!--Start Main Container-->
    <div class="container">
        <!--Start row-->
        <div class="row">
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
                    the_content();
                endwhile;
            else:
                ?>
            <?php endif; ?>
        </div><!--end row-->

        <!--Start row-->
        <div class="row">
            <div class="col-md-7">
                <div class="content-box">
                    <?php the_field('left_description') ?>
                </div><!--end content-box-->

                <div class="content-box">
                    <?php the_field('left_description2') ?>
                </div><!--end content-box-->
            </div><!--col-md-8-->

            <div class="col-md-4 float-right">
                <div class="right-block">
                    <?php the_field('at_a_glance') ?>
                </div>
            </div><!--end-col-md-4-->
        </div><!--end row--> 
    </div>	<!--end Main Container-->
</div><!--end row--> 



<!--Start-row-->
<div class="row-block result-block">
    <!--Start Main Container-->
    <div class="container">
        <h2>THE RESULTS</h2>
        <div class="row">
            <ul><?php
                    ?> <?php
                if ($post->ID == 97) {
                    query_posts(array('category_name' => 'the-results', 'post_type' => 'gba-results', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 4));
                }
                if ($post->ID == 414) {
                    query_posts(array('category_name' => 'the-results-for-member-profile', 'post_type' => 'gba-results', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 4));
                }
                //the loop start here
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <li>
                            <h3> <?php the_field('value'); ?></h3>
                            <span><?php the_content(); ?></span>
                        </li>
                        <?php
                    }
                }
                wp_reset_query();
                ?>         
            </ul>
        </div>
    </div><!--end Main Container-->
</div><!--end row-->



<!--Start-row-->
<div class="row-block views-block">
    <!--Start Main Container-->
    <div class="container">
        <?php
        if ($post->ID == 97) {
            query_posts(array('post_type' => 'uthumbs', 'p' => '653', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1));
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    ?>
            <?php the_content(); ?>
                    <figure>
                        <img src ="<?php the_field('profile_picture'); ?>">
                        </<figure>
                            <h5><?php the_field('name'); ?></h5>
                            <span>
                            <?php the_field('sub_title'); ?></span>
                        <?php
                        }
                    }
                    wp_reset_query();
                    //the loop start here
                }


                if ($post->ID == 414) {

                    query_posts(array('post_type' => 'uthumbs', 'p' => '651', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => 1));
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post();
                            the_content();
                            ?>
                            <figure><img src ="<?php the_field('profile_picture'); ?>"></figure>
                            <h5><?php the_field('name'); ?></h5>
                            <span><?php the_field('sub_title'); ?></span>
            <?php
        }
    }
    wp_reset_query();
}
?>    
                </div><!--end Main Container-->
                </div><!--end row-->




                <!--Start-row-->
                <div class="row-block social-block links-block">
                    <!--Start Main Container-->
                    <div class="container">
                        <h2>Follow Westpac</h2>
                        <ul class="links-list">

                            <li>
                                <figure>
<?php dynamic_sidebar('social-media-share'); ?>
                                </figure>
                            </li>

                        </ul>
                    </div><!--end Main Container-->
                </div>
<?php get_footer(); ?> 