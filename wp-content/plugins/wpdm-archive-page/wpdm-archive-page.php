<?php
/*
Plugin Name: WPDM - Archive Page
Description: Add arghive page option with wordpress download manager
Plugin URI: http://www.wpdownloadmanager.com/
Author: Shaon
Version: 2.4.6
Author URI: http://www.wpdownloadmanager.com/
*/

 
function wpdm_ap_css(){

    global $post;
    
    ?>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style type="text/css">
     
    div.w3eden .wpdm-all-categories div.cat-div{
        margin-bottom: 10px;
    }
    div.w3eden .wpdm-all-categories a.wpdm-pcat{
        font-weight: 800;
    }
    div.w3eden .wpdm-all-categories a.wpdm-scat{
        font-weight: 400;
        font-size: 9pt;
        margin-right: 10px;
        opacity: 0.6;
    }
    div.w3eden .wpdm-categories ul li,
    div.w3eden .wpdm-downloads ul li{
        list-style: none !important;
        list-style-type: none !important;
    }

    div.w3eden .wpdm-categories ul{
        list-style: none!important;
        padding: 0px !important;
        margin: 0px !important;
    }

    .dropdown-menu li,
    .w3eden .wpdm-downloads-ul li,
    .w3eden .wpdm-all-categories ul{
        padding: 0px !important;
        margin: 0px !important;
    }
    .w3eden .wpdm-categories ul.row li.col-md-4{
        margin-bottom: 5px !important;
        margin-left: 0 !important;
        padding: 4px;

    }
    .w3eden .btn-group.ap-btn > a:first-child:not(.btn-block){
        width: 82%;;
    }

    .wpdm-count{
        font-size:12px;
        color:#888;
    }
        #wpdm-downloads *{
            font-size: 10pt;
        }

     .w3eden #src{
         box-shadow: none;
         background: url(https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-ios7-search-strong-16.png) 15px center no-repeat;
         padding-left: 45px;
     }

     .w3eden .btn-ddm{
            position: absolute;z-index: 999999;margin-top: -29px;right: 5px;border:0 !important;
            -webkit-border-top-left-radius: 0px !important;-webkit-border-bottom-left-radius: 0px !important;-moz-border-radius-topleft: 0px !important;-moz-border-radius-bottomleft: 0px !important;border-top-left-radius: 0px !important;border-bottom-left-radius: 0px !important;
            background:rgba(0,0,0,0.05) !important;
        }

        .w3eden .panel-footer img{
            max-height: 30px;
        }

    .dropdown-submenu{position:relative;}
    .dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
    .dropdown-submenu:hover>.dropdown-menu{display:block;}
    .dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
    .dropdown-submenu:hover>a:after{border-left-color:#ffffff;}
    .dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
    .dropdown-menu li a{ padding: 7px 20px !important; }
    </style>
    <?php if(is_object($post)&&!strpos($post->post_content,'wpdm-archive')) return; ?>
    <script language="JavaScript">
    <!--
    function htmlEncode(value){
        //create a in-memory div, set it's inner text(which jQuery automatically encodes)
        //then grab the encoded contents back out.  The div never exists on the page.
        return jQuery('<div/>').text(value).html();
    }

    jQuery(function(){

         jQuery('.pagination a').live('click',function(e){
             e.preventDefault();
             jQuery('#wpdm-downloads').prepend('<div class="wpdm-loading"><i class="fa fa-spin fa-spinner icon icon-spin icon-spinner"></i> Loading...</div>').load(this.href);
         });
         
        jQuery('.wpdm-cat-link').click(function(e){
             e.preventDefault();
             jQuery('.wpdm-cat-link').removeClass('active');
             jQuery(this).addClass('active');

             var cat_id = jQuery(this).attr('rel');
             jQuery.ajax({
                type : "post",
                dataType : "json",
                url : '<?php echo admin_url('admin-ajax.php'); ?>',
                data : {action: "wpdm_change_cat_parent", cat_id : cat_id},
                success: function(response) {
                    console.log(response);
                   if(response.type == "success") {
                      jQuery('#inp').html(response.parent);
                   }
                }
             });
             
             //jQuery('#inp').html(jQuery(this).attr('cat_parent'));
             jQuery('#wpdm-downloads').prepend('<div class="wpdm-loading"><i class="fa fa-spin fa-spinner icon icon-spin icon-spinner"></i>  Loading...</div>').load('<?php echo home_url('/?wpdmtask=get_downloads&pg='.get_the_ID().'&category=');?>'+this.rel + '&order_by=' + jQuery('#order_by').val() +'&order=' + jQuery('#order').val());

         }); 
         
         jQuery('body').on('click', '.wpdm-cat-link2', function(e){
         //jQuery('.wpdm-cat-link').click(function(e){
             e.preventDefault();
             jQuery('.wpdm-cat-link').removeClass('active');
             var new_rel = jQuery(this).attr('test_rel');
             if( new_rel !== 'undefined') {
                 jQuery('a[rel=' + new_rel + ']').addClass('active');
             }
             
             var cat_id = jQuery(this).attr('rel');
             jQuery.ajax({
                type : "post",
                dataType : "json",
                url : '<?php echo admin_url('admin-ajax.php'); ?>',
                data : {action: "wpdm_change_cat_parent", cat_id : cat_id},
                success: function(response) {
                    console.log(response);
                   if(response.type == "success") {
                      jQuery('#inp').html(response.parent)
                   }
                }
             });
             
             //jQuery('#inp').html(jQuery(this).attr('cat_parent'));
             jQuery('#wpdm-downloads').prepend('<div class="wpdm-loading"><i class="fa fa-spin fa-spinner icon icon-spin icon-spinner"></i>  Loading...</div>').load('<?php echo home_url('/?wpdmtask=get_downloads&pg='.get_the_ID().'&category=');?>'+this.rel + '&order_by=' + jQuery('#order_by').val() +'&order=' + jQuery('#order').val());

         }); 
         
         
         jQuery('#srcp').submit(function(e){
             e.preventDefault();
             jQuery('.wpdm-cat-link').removeClass('active');
             var publish_date = encodeURIComponent(jQuery('#publish_date').val());
             var update_date = encodeURIComponent(jQuery('#update_date').val());
             jQuery('#inp').html('Search Result For <b>'+htmlEncode(jQuery('#src').val())+'</b>');
             jQuery('#wpdm-downloads').prepend('<div class="wpdm-loading"><i class="fa fa-spin fa-spinner icon icon-spin icon-spinner"></i>  Loading...</div>').load('<?php echo home_url('/?wpdmtask=get_downloads&pg='.get_the_ID().'&search=');?>'+encodeURIComponent(jQuery('#src').val() )+'&category='+jQuery('#initc').val() + '&order_by=' + jQuery('#order_by').val() +'&order=' + jQuery('#order').val() +'&search1[publish_date]='+publish_date +'&search1[update_date]='+update_date );

         });
          jQuery('#wpdmap-home').click(function(e){
              e.preventDefault();
              jQuery('.wpdm-cat-link').removeClass('active');
              jQuery('#inp').html('All Packages');
              jQuery('#wpdm-downloads').prepend('<div class="wpdm-loading"><i class="fa fa-spin fa-spinner icon icon-spin icon-spinner"></i> Loading...</div>').load('<?php echo home_url('/?wpdmtask=get_downloads&pg='.get_the_ID().'&search=');?>'+encodeURIComponent(jQuery('#src').val()) +'&category='+jQuery('#initc').val() + '&order_by=' + jQuery('#order_by').val() +'&order=' + jQuery('#order').val());
          });
         jQuery('#wpdm-downloads').prepend('<div class="wpdm-loading"><i class="fa fa-spin fa-spinner icon icon-spin icon-spinner"></i> Loading...</div>').load('<?php echo home_url('/?wpdmtask=get_downloads&pg='.get_the_ID().'&category=');?>'+encodeURIComponent(jQuery('#initc').val()));

         //jQuery('.wpdm-download-locked').live('click',function(){ alert(jQuery(this).parent().attr('class')); return false; });

      });
    //-->
    </script>
    <script language='JavaScript' type='text/javascript'>
        jQuery(function($) { 
            var update_date = false; 
            $('#update_date').focus(function(){
                if(update_date === false) {
                    $(this).daterangepicker({
                        format: 'YYYY-MM-DD',
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                            'Last 7 Days': [moment().subtract('days', 6), moment()],
                            'Last 30 Days': [moment().subtract('days', 29), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                         },
                         opens: 'left',
                         separator: ' to '
                         //startDate: moment().subtract('days', 29),
                         //endDate: moment()
                    });
                    update_date = true;
                }
            });
            var publish_date = false;
            $('#publish_date').focus(function(){
                if(publish_date === false) {
                    $(this).daterangepicker({
                        format: 'YYYY-MM-DD',
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                            'Last 7 Days': [moment().subtract('days', 6), moment()],
                            'Last 30 Days': [moment().subtract('days', 29), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                         },
                         opens: 'right',
                         separator: ' to '
                         //startDate: moment().subtract('days', 29),
                         //endDate: moment()
                        //});
                    });
                    publish_date = true;
                }
            });
        });
        </script>
    <?php
}

function wpdm_dir_page(){
    ob_start();
    ?>
    <style>
     .row-fluid .span3:nth-child(4n+1){
         margin-left:0px;
     }
    </style>
    <div class="wpdm-pro">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <select class="span11">
                <option value="">Browser by Category:</option>
                <?php wpdm_cat_dropdown_tree('',0,''); ?>  
               </select>
            </div>
            <div class="span6">
            <div class="input-append">
            <input type="text" class="input span10"><button class="btn"><i class="icon icon-search"></i></button>
            </div>
            </div>
        </div>
   
    <div class="row-fluid">
    <?php 
    global $wpdb;
    // where title like '%$src%' limit $start, $item_per_page
    $ndata = $wpdb->get_results("select * from {$wpdb->prefix}ahm_files",ARRAY_A);     
    $html = '';    
    foreach($ndata as $data){ 
        
        echo "<div class='span3'>".$data['title']."</div>";
        
    }
    ?>
    </div>
     </div>
    </div>
    <?php
    $html .= ob_get_clean();
    return $html;
}
   
    
function wpdm_front_render_cats($parent=0, $btype = 'default', $base = 0){
    global $wpdb, $current_user;    
    $user_role = isset($current_user->roles[0])?$current_user->roles[0]:'guest';

    $args = array(
        'orderby'       => 'name',
        'order'         => 'ASC',
        'hide_empty'    => false,
        'exclude'       => array(),
        'exclude_tree'  => array(),
        'include'       => array(),
        'number'        => '',
        'fields'        => 'all',
        'slug'          => '',
        'parent'         => $parent,
        'hierarchical'  => true,
        'get'           => '',
        'name__like'    => '',
        'pad_counts'    => false,
        'offset'        => '',
        'search'        => '',
        'cache_domain'  => 'core'
    );
    $categories = get_terms('wpdmcategory',$args);

    if(is_array($categories)){
    if($parent!=$base)   echo "<ul  class='dropdown-menu' role='menu' aria-labelledby='dLabel'>" ;
    foreach($categories as $category) {
        
        $cld = get_term_children( $category->term_id, 'wpdmcategory' );
        $ccount = $category->count;
        $link = get_term_link($category);

        ?>
     
    <li <?php if($parent==$base&&count($cld)>0){ ?>class="col-md-4 col-sm-6"<?php } else if(count($cld)>0){ ?>class="dropdown-submenu"<?php } elseif($parent==$base){ ?>class="col-md-4 col-sm-6"<?php } ?>>
                     
        <a class=" <?php if($parent==$base): ?>btn btn-sm btn-<?php echo $btype; ?> <?php if(count($cld)<=0||1): ?>btn-block<?php endif; ?><?php endif; ?> wpdm-cat-link" rel='<?php echo $category->term_id; ?>' href="<?php echo $link; ?>">
                    <?php echo stripcslashes($category->name); ?> (<?php echo $ccount; ?>)
                    </a> 
                    <?php if($parent==$base&&count($cld)>0): ?>
                    <a href="#" class="btn btn-ddm btn-sm btn-<?php echo $btype; ?> dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-angle-down"></i>
                    </a>  
                    <?php endif; ?>
        <?php wpdm_front_render_cats($category->term_id, $btype, $base);
 echo '</li>';
 }
 if($parent!=$base)   echo "</ul>" ;
 }
}
 
function wpdm_cats_vmenu($parent="",$level=0){
    global $wpdb, $current_user;    
    $user_role = $current_user->roles[0]?$current_user->roles[0]:'guest';
    if($categories = maybe_unserialize(get_option("_fm_categories",true))){    
    if(is_array($categories)){
    if($parent!='')   echo "<ul  class='nav nav-list bs-docs-sidenav affix' role='menu' aria-labelledby='dLabel'>" ;
    
    foreach($categories as $id=>$category) {
        if(@in_array($user_role, $category['access'])||$category['access']==''){
        $ccount = $wpdb->get_var("select count(*) as t from {$wpdb->prefix}ahm_files where category like '%\"$id\"%'");
        if($category['parent']==$parent){
            $pres = str_repeat("&mdash;", $level);
        ?>
     
    <li>
                     
                    <a rel='<?php echo $id; ?>' href="<?php echo wpdm_category_url($id); ?>">
                    <?php echo stripcslashes($category['title']); ?> (<?php echo $ccount; ?>)
                    </a> 
                     

 <?php wpdm_cats_vmenu($id,$level+1);
 echo '</li>';
 }}}
 if($parent!='')   echo "</ul>" ;
 }}}

function wpdm_ap_archive_page($params = array()){
    global $wpdb;
    @extract($params);

    $button_style = isset($button_style)?$button_style:'default';
    if(isset($category))
    {
        if((int)$category!==$category){
            $cat = get_term_by("slug", $category, "wpdmcategory");
            $category = $cat->term_id;
        }
    }
    //$initc = isset($params['category'])?$params['category']:"";
    $category = isset($category)?$category:0;
    $link_template = isset($link_template)?$link_template:'';
    $items_per_page = isset($items_per_page)?$items_per_page:0;
    update_post_meta(get_the_ID(),"__wpdm_link_template",$link_template);
    update_post_meta(get_the_ID(),"__wpdm_items_per_page",$items_per_page);

    if(isset($order)) {
        update_post_meta(get_the_ID(), '__wpdm_z_order', $order);
    }
    else {
        update_post_meta(get_the_ID(), '__wpdm_z_order', '');
    }
    
    if(isset($order_by)) {
        update_post_meta(get_the_ID(), '__wpdm_z_order_by', $order_by);
    }
    else {
        update_post_meta(get_the_ID(), '__wpdm_z_order_by', '');
    }
    
    if(isset($style)&&$style==1){
        
      return wpdm_dir_page();  
    } 
    
    $categories = maybe_unserialize(get_option('_fm_categories',array()));
    $pluginsurl = plugins_url();
    $dir = dirname(__FILE__);
    $url = WP_PLUGIN_URL . '/' . basename($dir);
    $extra_search = (isset($_GET['search1'])) ? array_map_recursive('stripslashes',$_GET['search1']) : array();
    $publish_date = (isset($extra_search['publish_date']) && $extra_search['publish_date'] != '') ? $extra_search['publish_date'] :"";
    $update_date = (isset($extra_search['update_date']) && $extra_search['update_date'] != '') ? $extra_search['update_date'] :"";
    $html = '
       
        <script src="'. $url . '/libs/daterangepicker/moment.min.js"></script>
        <script src="'. $url . '/libs/daterangepicker/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="'. $url . '/libs/daterangepicker/daterangepicker-bs3.css">
        
        
        <script src="'. $url . '/libs/slider/js/bootstrap-slider.js"></script>
        <link rel="stylesheet" type="text/css" href="'. $url . '/libs/slider/css/slider.css">
        
        
        <script src="'. $url . '/libs/bootstrap-select/bootstrap-select.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="'. $url . '/libs/bootstrap-select/bootstrap-select.min.css">
        <style>.w3eden .btn{ box-shadow: none !important; }</style>
    <div class=\'w3eden\'>
    <form id="srcp" style="margin-bottom: 10px">
    <div  class="row">
    <input type="hidden" name="category" id="initc" value="'.$category.'" />
        <span class="col-md-6">
            <label for="src">Search By Keyword:</label>
            <input type="text" class="form-control" name="src" placeholder="Search By Keyword" id="src">
        </span>
        <span class="col-md-3">
            <label for="order_by">Order By:</label>
            <select name="order_by" id="order_by" class="form-control selectpicker">
                <option value="view_count">View Count</option>
                <option value="download_count">Download Count</option>
                <!--<option value="package_size_b">Package Size</option>-->
                <option value="date">Publish Date</option>
                <option value="modified">Last Updated</option>
            </select>
        </span>

        <span class="col-md-3">
            <label for="order">Order:</label>
            <select name="order" id="order" class="form-control selectpicker">
                <option value="DESC">Descending Order</option>
                <option value="ASC">Ascending Order</option>
            </select>
        </span>

        </div><br class="clear"/>
        <div  class="row">
            <span class="form-group col-md-6">
                <label for="publish_date">Publish Date:</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" name="search[publish_date]" id="publish_date" class="form-control" value="'.$publish_date.'" />
                </div>
            </span>
            <span class="form-group col-md-6">
                <label for="update_date">Last Updated:</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="text" name="search[update_date]" id="update_date" class="form-control" value="'.$update_date.'" />
                </div>
            </span>
        </div>
        <br class="clear"/>
        <button class="btn btn-primary btn-large" type="submit" style="display:none">Search</button>
    </form>
   <div class="row">
    <div class="col-md-12">
        <div class="breadcrumb">
            <a href="#" id="wpdmap-home">Home</a> <i class="fa fa-angle-right icon icon-angle-right"></i> 
            <span id="inp">Document Types</span>
        </div>
    </div>
    </div>
    <div  class=\'wpdm-categories\'><ul class=\'row\'>';
    ob_start();
    wpdm_front_render_cats($category, $button_style, $category);
    $html .= ob_get_clean();

    $html .= "</ul><div class='clear'></div></div>
    <div class='clear'><br/></div>
    <div style='margin: 3px'>
    <div  class='wpdm-downloads row' id='wpdm-downloads'>
    Select category or search...
    </div></div></div> <script language='JavaScript' >jQuery('.dropdown-submenu a').dropdown();</script>
    ";
    
    return str_replace(array("\r","\n"),"",$html);
}

function wpdm_ap_categories($params = array()){
    global $wpdb;       
    @extract($params);
    $parent = isset($parent)?$parent:0;
    $args = array(
        'orderby'       => 'name',
        'order'         => 'ASC',
        'hide_empty'    => false,
        'exclude'       => array(),
        'exclude_tree'  => array(),
        'include'       => array(),
        'number'        => '',
        'fields'        => 'all',
        'slug'          => '',
        'parent'         => $parent,
        'hierarchical'  => false,
        'child_of'      => 0,
        'get'           => '',
        'name__like'    => '',
        'pad_counts'    => false,
        'offset'        => '',
        'search'        => '',
        'cache_domain'  => 'core'
    );
    $categories = get_terms('wpdmcategory',$args);
    $pluginsurl = plugins_url();
    $cols = isset($cols)&&$cols>0?$cols:2;
    $scols = intval(12/$cols);
    $icon = isset($icon)?"<style>.wpdm-all-categories li{background: url('{$icon}') left center no-repeat;}</style>":"";
    $k = 0;
    $html = "
    {$icon}    
    <div  class='wpdm-all-categories wpdm-categories-{$cols}col'><div class='row'>";
    foreach($categories as $id=>$category){
        $catlink = get_term_link($category);
        if($category->parent==$parent) {
        $ccount = $category->count;
        if(isset($showcount)&&$showcount) $count  = "&nbsp;<span class='wpdm-count'>($ccount)</span>";
            $html .= "<div class='col-md-{$scols} cat-div'><a class='wpdm-pcat' href='$catlink' >".htmlspecialchars(stripslashes($category->name))." (".$category->count.")</a>";
            if(isset($subcat) && $subcat==1) {
                $sargs = array(
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => false,
                    'fields' => 'all',
                    'hierarchical' => false,
                    'child_of' => $category->term_id,
                    'pad_counts' => false
                );
                $subcategories = get_terms('wpdmcategory', $sargs);
                $html .= "<div class='wpdm-subcats'>";
                foreach ($subcategories as $sid => $subcategory) {
                    $scatlink = get_term_link($subcategory);
                    $html .= "<a  class='wpdm-scat' href='$scatlink' >" . htmlspecialchars(stripslashes($subcategory->name)) . " (".$subcategory->count.") </a>";
                }
                $html .= "</div>";
            }
            $html .= "</div>";
        $k++;
        }
    }
    
    $html .= "</div><div style='clear:both'></div></div>";
    if($k==0) $html = '';
    return "<div class='w3eden'>".str_replace(array("\r","\n"),"",$html)."</div>";
}

//__wpdm_access
//guest || ... 
function wpdm_get_downloads(){

    if(!isset($_GET['wpdmtask'])||$_GET['wpdmtask']!='get_downloads') return;

    global $wpdb, $current_user;    
    get_currentuserinfo();

    $actpl =  get_option('_wpdm_ap_search_page_template','link-template-default.php');
    $tctpl = get_post_meta((int)$_GET['pg'],'__wpdm_link_template', true);
    $item_per_page = get_post_meta((int)$_GET['pg'],'__wpdm_items_per_page', true);
    
    
    
    if($tctpl!='') $actpl = $tctpl;

    $category = isset($_REQUEST['category'])?addslashes($_REQUEST['category']):'';
    $src = isset($_GET['search'])?addslashes($_GET['search']):'';

    $item_per_page =  $item_per_page<=0?10:$item_per_page;

    $page = isset($_GET['cp'])?$_GET['cp']:1;
    $start = ($page-1)*$item_per_page;
    $params = array("post_type"=>"wpdmpro","posts_per_page"=>$item_per_page,"offset"=>$start);
    
    //order parameter
    $order = get_post_meta((int)$_GET['pg'],'__wpdm_z_order',true);
    $order_by = get_post_meta((int)$_GET['pg'],'__wpdm_z_order_by',true);
    //echo $order . ' ' . $order_by . '<br>';
    //
    $order = isset($_GET['order']) ? addslashes($_GET['order']) : $order;
    $order_by = isset($_GET['order_by']) ? addslashes($_GET['order_by']) : $order_by;
    //echo $order . ' ' . $order_by . '<br>';
    
    if(isset($order_by) && $order_by != '') {
        //order parameter
        if($order_by == 'view_count' || $order_by == 'download_count' || $order_by == 'package_size_b'){
            $params['meta_key'] = '__wpdm_' . $order_by;
            $params['orderby'] = 'meta_value_num'; 
        }
        else {
            $params['orderby'] = $order_by;
        }
        if($order == '') $order = 'ASC';
        $params['order'] = $order;
        
    }
    
    if($category)
        $params['tax_query'] = array(array(
        'taxonomy' => 'wpdmcategory',
        'field' => 'id',
        'terms' => $category
    ));



    if($src!=''){
        $params['s'] = $src;
    }
    //for publish_date and update_date
    if(isset($_GET['search1'])){
        $extra_search = (isset($_GET['search1'])) ? array_map_recursive('stripslashes',$_GET['search1']) : array();
        if ($extra_search['publish_date'] != '') {
                $publish_dates = explode(' to ', $extra_search['publish_date']);
                $params['date_query'][] = array(
                    'column' => 'post_date_gmt',
                    'after' => $publish_dates[0],
                    'before' => $publish_dates[1],
                    'inclusive' => true,
                );
            }

        if ($extra_search['update_date'] != '') {
            $update_dates = explode(' to ', $extra_search['update_date']);
            $params['date_query'][] = array(
                'column' => 'post_modified_gmt',
                'after' => $update_dates[0],
                'before' => $update_dates[1],
                'inclusive' => true,
            );
        }
    }
    
    
    
    $q = new WP_Query($params);
    //echo "<pre>"; print_r($q); echo "</pre>";
    $total = $q->found_posts;

    $pages = ceil($total/$item_per_page);
    $pag = new wpdm_pagination();             
    $pag->changeClass('wpdm-ap-pag');
    $pag->items($total);
    $pag->limit($item_per_page);
    $pag->currentPage($page);
    $url = strpos($_SERVER['REQUEST_URI'],'?')?$_SERVER['REQUEST_URI'].'&':$_SERVER['REQUEST_URI'].'?';
    $url = preg_replace("/\&cp=[0-9]+/","",$url);
    $pag->urlTemplate($url."cp=[%PAGENO%]");

    $html = '';
    $role = @array_shift(array_keys($current_user->caps));
    
    while ($q->have_posts()){ $q->the_post();
        $package_role = maybe_unserialize(get_post_meta(get_the_ID(), '__wpdm_access', true));
        //if(is_array($package_role) && !in_array('guest', $package_role) && !in_array($role, $package_role)) {
        //    continue;
        // }
        $ext = "_blank";
        $data = wpdm_custom_data(get_the_ID());
        $data += get_post(get_the_ID(), ARRAY_A);
        $data['download_count'] = isset($data['download_count'])?$data['download_count']:0;
        $data['ID'] = get_the_ID();
        $data['id'] = get_the_ID();
        if(isset($data['files'])&&count($data['files'])>0){
            $tmpvar = explode(".",$data['files'][0]);
            $ext = count($tmpvar) > 1 ? end($tmpvar) : $ext;
        }
      
        $link_label = isset($data['link_label'])?stripslashes($data['link_label']):'Download';

        $data['page_url'] = get_permalink(get_the_ID());

      
                      
        $templates = maybe_unserialize(get_option("_fm_link_templates",true));        
             
                   
                $data['files'] = isset($data['files']) ? maybe_unserialize($data['files']):array();
               
                
                if(isset($templates[$actpl]['content'])&&$templates[$actpl]['content']!='') $actpl = $templates[$actpl]['content'];   
               
                $repeater = FetchTemplate($actpl, $data, 'link');
                
                $html .= $repeater;
              
                
    
            
            
            //}   
            
     
            
    }
     
    if($total==0) $html = 'No download found!';    
    echo str_replace(array("\r","\n"),"","$html<div class='clear'></div>".$pag->show()."<div class='clear'></div>");
    die();
}

function wpdm_ap_seach_bar($params = array()){
    @extract($params);
    ob_start();
    $dir = dirname(__FILE__);
    $url = WP_PLUGIN_URL . '/' . basename($dir);
    $extra_search = (isset($_GET['search'])) ? array_map_recursive('stripslashes',$_GET['search']) : array();
    $src = isset($_GET['q']) ? addslashes($_GET['q']): '' ;
    //if($extra_search)        print_r($extra_search);
    $button_style = isset($button_style)?$button_style:'default';
    $link_template = isset($link_template)?$link_template:'';
    $cols = isset($cols)&&$cols>0?$cols:2;
    $cols = 'col-md-'.intval(12/$cols);

    ?>
        <!-- Date Range Picker -->
        <script src='<?php echo $url . '/libs/daterangepicker/moment.min.js' ?>'></script>
        <script src='<?php echo $url . '/libs/daterangepicker/daterangepicker.js' ?>'></script>
        <link rel="stylesheet" type="text/css" href='<?php echo $url . '/libs/daterangepicker/daterangepicker-bs3.css' ?>'>
        
        <!-- Range Slider -->
        <script src='<?php echo $url . '/libs/slider/js/bootstrap-slider.js' ?>'></script>
        <link rel="stylesheet" type="text/css" href='<?php echo $url . '/libs/slider/css/slider.css' ?>'>
        
        <!-- Bootstrap Multi Select -->
        <script src='<?php echo $url . '/libs/bootstrap-select/bootstrap-select.min.js' ?>'></script>
        <link rel="stylesheet" type="text/css" href='<?php echo $url . '/libs/bootstrap-select/bootstrap-select.min.css' ?>'>
        <style>.w3eden .btn{ box-shadow: none !important; }</style>
    <div class="w3eden">

            <form action="" class="well">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="package_size">Search By Keyword:</label>
                        <input type="text" class="form-control" placeholder="Looking For..." name="q" id="s" value="<?php echo $src; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="publish_date">Publish Date:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" name="search[publish_date]" id="publish_date" class="form-control" value="<?php if(isset($extra_search['publish_date']) && $extra_search['publish_date'] != '') { echo $extra_search['publish_date']; } ?>" />
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="update_date">Last Updated:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" name="search[update_date]" id="update_date" class="form-control" value="<?php if(isset($extra_search['update_date']) && $extra_search['update_date'] != '') { echo $extra_search['update_date']; } ?>" />
                        </div>
                    </div>

                    <script type="text/javascript">
                        jQuery(function($) {
                            var update_date = false;
                            $('#update_date').focus(function(){
                                if(update_date === false) {
                                    $(this).daterangepicker({
                                        format: 'YYYY-MM-DD',
                                        ranges: {
                                            'Today': [moment(), moment()],
                                            'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                            'Last 7 Days': [moment().subtract('days', 6), moment()],
                                            'Last 30 Days': [moment().subtract('days', 29), moment()],
                                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                                            'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                                         },
                                         opens: 'left',
                                         separator: ' to '
                                         //startDate: moment().subtract('days', 29),
                                         //endDate: moment()
                                    });

                                    update_date = true;
                                }
                            });

                            var publish_date = false;
                            $('#publish_date').focus(function(){
                                if(publish_date === false) {
                                    $(this).daterangepicker({
                                        format: 'YYYY-MM-DD',
                                        ranges: {
                                            'Today': [moment(), moment()],
                                            'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                            'Last 7 Days': [moment().subtract('days', 6), moment()],
                                            'Last 30 Days': [moment().subtract('days', 29), moment()],
                                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                                            'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                                         },
                                         opens: 'right',
                                         separator: ' to '
                                         //startDate: moment().subtract('days', 29),
                                         //endDate: moment()
                                        //});
                                    });

                                    publish_date = true;
                                }
                            });


                        });
                    </script>
                </div>
                <!--<div class="row">
                    <div class="form-group col-md-6">
                        <label for="view_count">View Count >= :</label> <br>
                        <input type="text" name="search[view_count]" id="view_count" class="slider" data-slider-min="0" <?php if(isset($extra_search['view_count']) && $extra_search['view_count'] != '') { echo "data-slider-value='{$extra_search['view_count']}' value='{$extra_search['view_count']}'"; } ?> />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="download_count">Download Count >= :</label> <br>
                        <input type="text" name="search[download_count]" id="download_count" class="slider" data-slider-min="0" <?php if(isset($extra_search['download_count']) && $extra_search['download_count'] != '') { echo "data-slider-value='{$extra_search['download_count']}' value='{$extra_search['download_count']}'"; } ?> />
                    </div>
                    <script type="text/javascript">
                        jQuery(function($){
                            $('.slider').slider({max:1000});
                        });
                    </script>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="package_size">Package Size (Bytes) >= :</label> <br>
                        <input type="text" name="search[package_size]" id="package_size" data-slider-min="0" data-slider-max="10000" class="slider col-md-12" <?php if(isset($extra_search['package_size']) && $extra_search['package_size'] != '') { echo "data-slider-value='{$extra_search['package_size']}' value='{$extra_search['package_size']}'"; } ?> />
                    </div>
                </div>-->

                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="category">Document Type :</label>
                        <?php
                        $args = array(
                            'orderby'       => 'name',
                            'order'         => 'ASC',
                            'hide_empty'    => true,
                            'exclude'       => array(),
                            'exclude_tree'  => array(),
                            'include'       => array(),
                            'number'        => '',
                            'fields'        => 'all',
                            'slug'          => '',
                            'hierarchical'  => true,
                            'child_of'      => 0,
                            'get'           => '',
                            'name__like'    => '',
                            'pad_counts'    => false,
                            'offset'        => '',
                            'search'        => '',
                            'cache_domain'  => 'core'
                        );
                        $terms = get_terms('wpdmcategory',$args);
                        $options = "";
                        if ( !empty( $terms ) && !is_wp_error( $terms ) ){
                            foreach ( $terms as $term ) {
                                if(isset($extra_search['category']) && in_array($term->term_id, $extra_search['category']))
                                        $selected ='selected="selected"';
                                else $selected = '';
                                $options .= "<option value='{$term->term_id}' $selected>" . $term->name . "</option>";
                            }
                        }
                        ?>

                        <select name='search[category][]' class="form-control selectpicker" multiple="multiple" id="category">
                            <?php echo $options; ?>
                        </select>
                    </div>
                    <script type="text/javascript">
                        jQuery(function($){
                            $('.selectpicker').selectpicker({'noneSelectedText':'Nothing Selected'});
                        });
                    </script>
                </div>

                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="order_by">Order By:</label> <br>
                        <select name="search[order_by]" class="form-control selectpicker" id="order_by">
                            <option value="">Nothing Selected</option>
                            <option value="__wpdm_view_count">View Count</option>
                            <option value="__wpdm_download_count">Download Count</option>
                            <!--<option value="__wpdm_package_size_b">Package Size</option>-->
                            <option value="date">Publish Date</option>
                            <option value="modified">Last Updated</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="order">Order:</label> <br>
                        <select name="search[order]" class="form-control selectpicker" id="order">
                            <option value="ASC">Ascending Order</option>
                            <option value="DESC">Descending Order</option>
                        </select>
                    </div>
                </div>

                
                <button class="btn btn-primary btn-large" type="submit">Search</button>
                <button class="btn btn-warning btn-large" type="reset">Reset</button>
            </form>

    <div class="row">
    <?php

    if(isset($_GET['q'])) {

        global $wpdb, $current_user;
        get_currentuserinfo();


        $tax_query = array();

        //date meta
        if ($extra_search['publish_date'] != '') {
            $publish_dates = explode(' to ', $extra_search['publish_date']);
            $tax_query['date_query'][] = array(
                'column' => 'post_date_gmt',
                'after' => $publish_dates[0],
                'before' => $publish_dates[1],
                'inclusive' => true,
            );
        }

        if ($extra_search['update_date'] != '') {
            $update_dates = explode(' to ', $extra_search['update_date']);
            $tax_query['date_query'][] = array(
                'column' => 'post_modified_gmt',
                'after' => $update_dates[0],
                'before' => $update_dates[1],
                'inclusive' => true,
            );
        }


        //post meta query
        if ($extra_search['view_count'] != '' || $extra_search['view_count'] > 0) {
            $tax_query['meta_query'][] = array(
                'key' => '__wpdm_view_count',
                'value' => $extra_search['view_count'],
                'compare' => '>='
            );
        }

        if ($extra_search['download_count'] != '' || $extra_search['download_count'] > 0) {
            $tax_query['meta_query'][] = array(
                'key' => '__wpdm_download_count',
                'value' => $extra_search['download_count'],
                'compare' => '>='
            );
        }

        if ($extra_search['package_size'] != '' || $extra_search['package_size'] > 0) {
            $tax_query['meta_query'][] = array(
                'key' => '__wpdm_package_size_b',
                'value' => $extra_search['package_size'],
                'compare' => '>='
            );
        }

        //order parameter
        if ($extra_search['order_by'] != '') {
            if ($extra_search['order_by'] != 'modified' && $extra_search['order_by'] != 'date') {
                $tax_query['meta_key'] = $extra_search['order_by'];
                $tax_query['orderby'] = 'meta_value_num';
            } else {
                $tax_query['orderby'] = $extra_search['order_by'];
            }

            $tax_query['order'] = $extra_search['order'];


        }

        //category parameter
        if (isset($extra_search['category']) && !empty($extra_search['category'])) {
            $tax_query['tax_query'][] = array(
                'taxonomy' => 'wpdmcategory',
                'field' => 'term_id',
                'terms' => $extra_search['category'],
                'operator' => 'IN',
                'include_children' => false
            );
        }

        //search parameter

        if ($src != '') {
            $tax_query['s'] = $src;
        }

        //template select
        $pg = isset($_GET['pg']) ? (int)$_GET['pg'] : 0;
        $actpl = get_option('_wpdm_ap_search_page_template', 'link-template-default.php');

        if ($link_template != '') $actpl = $link_template;

        //post_type and pagination parameter
        $items_per_page = !isset($items_per_page) || $items_per_page <= 0 ? 10 : $items_per_page;
        $page = isset($_GET['cp']) ? $_GET['cp'] : 1;
        $start = ($page - 1) * $items_per_page;
        $tax_query["post_type"] = "wpdmpro";
        $tax_query["posts_per_page"] = $items_per_page;
        $tax_query["offset"] = $start;

        $q = new WP_Query($tax_query);
        //print_r($q);
        $total = $q->found_posts;

        //pagination
        $pages = ceil($total / $items_per_page);
        $pag = new wpdm_pagination();
        $pag->changeClass('wpdm-ap-pag');
        $pag->items($total);
        $pag->limit($items_per_page);
        $pag->currentPage($page);
        $url = strpos($_SERVER['REQUEST_URI'], '?') ? $_SERVER['REQUEST_URI'] . '&' : $_SERVER['REQUEST_URI'] . '?';
        $url = preg_replace("/\&cp=[0-9]+/", "", $url);
        $pag->urlTemplate($url . "cp=[%PAGENO%]");

        $html = '';

        $role = @array_shift(array_keys($current_user->caps));

        while ($q->have_posts()) {
            $q->the_post();
            $package_role = maybe_unserialize(get_post_meta(get_the_ID(), '__wpdm_access', true));
            if (is_array($package_role) && !in_array('guest', $package_role) && !in_array($role, $package_role)) {
                continue;
            }

            $ext = "_blank";
            $data = wpdm_custom_data(get_the_ID());
            $data += get_post(get_the_ID(), ARRAY_A);

            $data['download_count'] = isset($data['download_count']) ? $data['download_count'] : 0;
            $data['ID'] = get_the_ID();
            $data['id'] = get_the_ID();
            if (isset($data['files']) && count($data['files']) > 0) {
                $tmpvar = explode(".", $data['files'][0]);
                $ext = count($tmpvar) > 1 ? end($tmpvar) : $ext;
            }

            $link_label = isset($data['link_label']) ? stripslashes($data['link_label']) : 'Download';

            $data['page_url'] = get_permalink(get_the_ID());


            $role = @array_shift(array_keys($current_user->caps));
            $templates = maybe_unserialize(get_option("_fm_link_templates", true));


            $data['files'] = isset($data['files']) ? maybe_unserialize($data['files']) : array();


            if (isset($templates[$actpl]['content']) && $templates[$actpl]['content'] != '') $actpl = $templates[$actpl]['content'];

            $repeater = FetchTemplate($actpl, $data, 'link');

            $html .= "<div class='{$cols}'>" . $repeater . "</div>";

        }

        if ($total == 0) $html = 'No download found!';
        echo str_replace(array("\r", "\n"), "", "$html<div class='clear'></div>" . $pag->show() . "<div class='clear'></div>");
    }
    ?>
    </div>
    </div>

    <?php
    $data = ob_get_clean();
    return $data;
}

function wpdm_ac_link_template(){
    ?>
    <tr>
        <td>Link Template for Archive Page</td>
        <td><select name="_wpdm_ap_search_page_template" id="ltac">
                <?php 
                  $actpl = get_option("_wpdm_ap_search_page_template",'link-template-default.php');
                  $ctpls = scandir(WPDM_BASE_DIR.'/templates/');
                  array_shift($ctpls);
                  array_shift($ctpls);
                  foreach($ctpls as $ctpl){
                      $tmpdata = file_get_contents(WPDM_BASE_DIR.'/templates/'.$ctpl);
                      if(preg_match("/WPDM[\s]+Link[\s]+Template[\s]*:([^\-\->]+)/",$tmpdata, $matches)){                           
                ?>                
                <option value="<?php echo $ctpl; ?>"  <?php echo ( $actpl==$ctpl )?' selected ':'';  ?>><?php echo $matches[1]; ?></option>
                <?php
                }}
                
                $templates = unserialize(get_option("_fm_link_templates",true));
 
                  foreach($templates as $id=>$template) {  
                ?>
                <option value="<?php echo $id; ?>"  <?php echo ( $actpl==$id )?' selected ':'';  ?>><?php echo $template['title']; ?></option>
                <?php }  ?>
                </select></td>
    </tr>
    <?php
    
} 

class wpdm_search_widget extends WP_Widget {
    /** constructor */
    function wpdm_search_widget() {
        parent::WP_Widget(false, 'WPDM Search');
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        $url = get_permalink($instance['rpage']);
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; 
                echo "<form action='".$url."' class='wpdm-pro'>";        
                echo "<div class='input-append'><input type=text name='q' /><button class='btn'><i class='icon icon-search'></i>Search</button></div><div class='clear'></div>";
                echo "</form>";
               echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['rpage'] = strip_tags($new_instance['rpage']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
        $title = esc_attr($instance['title']);
        $rpage = esc_attr($instance['rpage']);
        
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
          <?php echo "Search Result Page:<br/>".wp_dropdown_pages("selected={$rpage}&echo=0&name=".$this->get_field_name('rpage'));  ?>
        </p>
        <div style="border:1px solid #cccccc;padding:10px;border-radius:4px;font-size:8pt">
        Note: Create a page with short-code <b>[wpdm-search-page]</b> and select that page as search redult page
        </div>
        <?php 
    }

} 

if(!function_exists('array_map_recursive')) {
    function array_map_recursive($callback, $value){
        if (is_array($value)) {
            return array_map(function($value) use ($callback) { return array_map_recursive($callback, $value); }, $value);
        }
        return $callback($value);
    }
}

function wpdm_ap_tags($params = array()){
    global $wpdb;
    @extract($params);
    $parent = isset($parent)?$parent:0;
    $args = array(
        'orderby'       => 'name',
        'order'         => 'ASC',
        'hide_empty'    => false,
        'exclude'       => array(),
        'exclude_tree'  => array(),
        'include'       => array(),
        'number'        => '',
        'fields'        => 'all',
        'slug'          => '',
        'parent'         => $parent,
        'hierarchical'  => true,
        'child_of'      => 0,
        'get'           => '',
        'name__like'    => '',
        'pad_counts'    => false,
        'offset'        => '',
        'search'        => '',
        'cache_domain'  => 'core'
    );
    $categories = get_terms('post_tag',$args);
    $pluginsurl = plugins_url();
    $cols = isset($cols)&&$cols>0?$cols:2;
    $scols = intval(12/$cols);
    $icon = isset($icon)?"<i class='fa fa-{$icon}'></i>":"<i class='fa fa-tag'></i>";
    $btnstyle = isset($btnstyle)?$btnstyle:'success';
    $k = 0;
    $html = "

    <div  class='wpdm-all-categories wpdm-categories-{$cols}col'><ul class='row'>";
    foreach($categories as $id=>$category){
        $catlink = get_term_link($category);
        if($category->parent==$parent) {
            $ccount = $category->count;
            if(isset($showcount)&&$showcount) $count  = "&nbsp;<span class='wpdm-count'>($ccount)</span>";
            $html .= "<div class='col-md-{$scols} col-tag'><a class='btn btn-{$btnstyle} btn-block text-left' href='$catlink' >{$icon} &nbsp; ".htmlspecialchars(stripslashes($category->name))."</a></div>";
            $k++;
        }
    }

    $html .= "</ul><div style='clear:both'></div></div><style>.col-tag{ margin-bottom: 10px !important; } .col-tag .btn{ text-align: left !important; padding-left: 10px !important; box-shadow: none !important; }</style>";
    if($k==0) $html = '';
    return "<div class='w3eden'>".str_replace(array("\r","\n"),"",$html)."</div>";
}

function get_custom_category_parents( $id, $taxonomy = false, $link = false, $separator = '/', $nicename = false, $visited = array() ) {

	if(!($taxonomy && is_taxonomy_hierarchical( $taxonomy )))
		return '';

	$chain = '';
	// $parent = get_category( $id );
	$parent = get_term( $id, $taxonomy);
	if ( is_wp_error( $parent ) )
		return $parent;

	if ( $nicename )
		$name = $parent->slug;
	else
		$name = $parent->name;

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
		$visited[] = $parent->parent;
		// $chain .= get_category_parents( $parent->parent, $link, $separator, $nicename, $visited );
		$chain .= get_custom_category_parents( $parent->parent, $taxonomy, $link, $separator, $nicename, $visited );
	}

	if ( $link ) {
		// $chain .= '<a href="' . esc_url( get_category_link( $parent->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>' . $separator;
		$chain .= '<a href="' . esc_url( get_term_link( (int) $parent->term_id, $taxonomy ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>' . $separator;
	} else {
		$chain .= $name.$separator;
	}
	return $chain;
}

function wpdm_change_cat_parent(){
    $cat_id = isset($_REQUEST['cat_id']) ? (int) $_REQUEST['cat_id'] : '';
    $result['type'] = 'failed';
    if(is_numeric($cat_id)) {
        $result['type'] = 'success';
        
        $parents = rtrim(get_custom_category_parents($cat_id,'wpdmcategory',false,'>',false),'>');
        $temp = explode('>', $parents);
        //print_r($temp);
        $count = count($temp);
        $str = "";
        for($i = 1; $i<=$count ; $i++){
            if($i == $count) {
                $str .= "{$temp[$i-1]}";
            }
            else {
                
                $parent = get_term_by('name', $temp[$i-1], 'wpdmcategory');
                //print_r($parent);
                $link = get_term_link($parent);
                //print_r($link);
                $a = "<a class='wpdm-cat-link2' rel='{$parent->term_id}' test_rel='{$parent->term_id}' title='{$parent->description}' href='$link'>{$parent->name}</a> > ";
                $str .= $a;
            }
        }
        $result['parent'] = $str;
        
    }
    
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    die();
}

function wpdm_arcsc_generate(){
    ?>

    <div class="panel panel-default">
    <div class="panel-heading">Archive Page</div>
    <div class="panel-body">
        <?php wpdm_dropdown_categories('c',0, 'apc'); ?>
        <select style="margin-right: 5px;" id="btns_ap">
            <option value="default">Button:</option>
            <option value="default">Default</option>
            <option value="success">Green</option>
            <option value="blue">Blue</option>
            <option value="warning">Yellow</option>
            <option value="danger">Red</option>
            <option value="info">Cyan</option>
        </select>
        <select style="margin-right: 5px;width: 180px" id="plnk_tpl_ap">
            <option value="link-template-default.php"><?php echo __('Link Template:', 'wpdmpro'); ?></option>
            <?php
            $ctpls = scandir(WPDM_BASE_DIR . '/templates/');
            array_shift($ctpls);
            array_shift($ctpls);
            $ptpls = $ctpls;
            foreach ($ctpls as $ctpl) {
                $tmpdata = file_get_contents(WPDM_BASE_DIR . '/templates/' . $ctpl);
                if (preg_match("/WPDM[\s]+Link[\s]+Template[\s]*:([^\-\->]+)/", $tmpdata, $matches)) {

                    ?>
                    <option
                        value="<?php echo str_replace(".php","",$ctpl); ?>" ><?php echo $matches[1]; ?></option>
                <?php
                }
            }
            if ($templates = unserialize(get_option("_fm_link_templates", true))) {
                foreach ($templates as $id => $template) {
                    ?>
                    <option
                        value="<?php echo $id; ?>" ><?php echo $template['title']; ?></option>
                <?php }
            } ?>
        </select><br style="display: block;clear: both;margin-top: 5px"/>
        <select id="acob" style="margin-right: 5px">
            <option value="post_title">Order By:</option>
            <option value="post_title">Title</option>
            <option value="download_count">Downloads</option>
            <option value="package_size_b">Package Size</option>
            <option value="view_count">Views</option>
            <option value="date">Publish Date</option>
            <option value="modified">Update Date</option>
        </select><select id="acobs" style="margin-right: 5px">
            <option value="asc">Order:</option>
            <option value="asc">Asc</option>
            <option value="desc">Desc</option>
        </select>
        <button class="btn btn-primary btn-sm" id="acps">Insert to Post</button>
        <script>
            jQuery('#acps').click(function(){

                var cats = jQuery('#apc').val()!='-1'?' category="' + jQuery('#scc').val() + '" ':'';
                var bts = ' button_style="' + jQuery('#btns_ap').val() + '" ';
                var linkt = ' link_template="' + jQuery('#plnk_tpl_ap').val() + '" ';
                var acob = ' order_by="' + jQuery('#acob').val() + '" order="' + jQuery('#acobs').val() + '"';
                var win = window.dialogArguments || opener || parent || top;
                win.send_to_editor('[wpdm-archive' + cats + bts + linkt + acob + ' items_per_page="10"]');
                tinyMCEPopup.close();
                return false;
            });
        </script>
    </div>
        <div class="panel-heading">Categories</div>
        <div class="panel-body">
            <select id="spc" style="margin-right: 5px">
                <option value="1">Package Count:</option>
                <option value="1">Show</option>
                <option value="0">Hide</option>
            </select><select id="ssc" style="margin-right: 5px">
                <option value="1">Sub Cats:</option>
                <option value="1">Show</option>
                <option value="0">Hide</option>
            </select><select id="apcols" style="margin-right: 5px">
                <option value="3">Cols:</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
            <button class="btn btn-primary btn-sm" id="apcts">Insert to Post</button>
            <script>
                jQuery('#apcts').click(function(){

                    var scats =' subcat="' + jQuery('#ssc').val() + '" ';
                    var count = ' showcount="' + jQuery('#spc').val() + '" ';
                    var cols = ' cols="' + jQuery('#apcols').val() + '" ';
                    var win = window.dialogArguments || opener || parent || top;
                    win.send_to_editor('[wpdm-categories' + scats + count + cols + ']');
                    tinyMCEPopup.close();
                    return false;
                });
            </script>
        </div>
    <div class="panel-heading">More...</div>
    <div class="panel-body">
        <select id="apms" style="margin-right: 5px">
            <option value='[wpdm-tags cols="4" icon="tag"  btnstyle="default"]'>Tags</option>
            <option value='[wpdm-search-page cols="1" items_per_page="10" link_template="link-template-panel"]'>Advanced Search</option>
        </select>
        <button class="btn btn-primary btn-sm" id="apmsb">Insert to Post</button>
        <script>
            jQuery('#apmsb').click(function(){
                var win = window.dialogArguments || opener || parent || top;
                win.send_to_editor(jQuery('#apms').val());
                tinyMCEPopup.close();
                return false;
            });
        </script>
        </div>
    </div>

    <?php
}

function wpdm_ap_hierarchical_filter($params = array()){
    if(is_array($params)) extract($params);
    $link_template = isset($link_template)?$link_template:'';
    $items_per_page = isset($items_per_page)?$items_per_page:0;

    update_post_meta(get_the_ID(),"__wpdm_link_template",$link_template);
    update_post_meta(get_the_ID(),"__wpdm_items_per_page",$items_per_page);


    ?>
        <div class="w3eden">
                <div class="row">
                    <div class="col-md-3">
                        <label for="bu"><?php _e('Business Unit','wpdmpro'); ?></label>
                        <select id="bu" class="form-control" name="bu">
                            <?php
                                $bu = get_terms('wpdmcategory', array('hide_empty'=> false,'parent'=>0));
                                foreach($bu as $term){
                                    echo "<option value='{$term->term_id}'>{$term->name}</option>";
                                }
                            ?>
                        </select>

                    </div>
                    <div class="col-md-3">
                        <label for="bu"><?php _e('Business Unit Area','wpdmpro'); ?></label>
                        <select id="bua" class="form-control" name="bua">

                        </select>

                    </div>
                    <div class="col-md-3">
                        <label for="bu"><?php _e('Brand Element','wpdmpro'); ?></label>
                        <select id="be"class="form-control" name="be">

                        </select>

                    </div>
                    <div class="col-md-3">
                        <label for="bu"><?php _e('Search Keyword','wpdmpro'); ?></label>
                        <input type="search" class="form-control" />

                    </div>
                </div>
                <hr/>
                <div id="adls">

                </div>
        </div>
        <script>

            jQuery(function($){
                $('#bu').on('change', function(){
                    $('#bua').html('<option>Loading...</option>').load('?wpdmtask=wpdm_ap_get_child_cats&parent='+$(this).val())
                });

                $('#bua').on('change', function(){
                    $('#be').html('<option>Loading...</option>').load('?wpdmtask=wpdm_ap_get_child_cats&parent='+$(this).val())
                });

                $('#be').on('change', function(){
                    $('#adls').html('<option>Loading...</option>').load('?wpdmtask=get_downloads&pg=<?php the_ID(); ?>&category='+$(this).val())
                });

            });

        </script>
    <?php
}

function wpdm_ap_get_child_cats(){
    if(isset($_REQUEST['wpdmtask']) && $_REQUEST['wpdmtask'] == 'wpdm_ap_get_child_cats'){
        $bu = get_terms('wpdmcategory', array('hide_empty'=> false, 'parent'=>(int)$_REQUEST['parent']));
        echo (count($bu)>0)?'<option value="">Select</option>':'<option value="">Nothing here</option>';
        foreach($bu as $term){
            echo "<option value='{$term->term_id}'>{$term->name}</option>";
        }

        die();
    }
}

add_action('wpdm_ext_shortcode', 'wpdm_arcsc_generate');
add_action("wp_ajax_wpdm_change_cat_parent", "wpdm_change_cat_parent");
add_action("init", "wpdm_ap_get_child_cats");
add_action("wp_ajax_nopriv_wpdm_change_cat_parent", "wpdm_change_cat_parent");
add_action('basic_settings','wpdm_ac_link_template');
add_action('wp_loaded','wpdm_get_downloads');
add_action('wp_head','wpdm_ap_css');
add_shortcode( 'wpdm-hrc', 'wpdm_ap_hierarchical_filter');
add_shortcode( 'wpdm-archive', 'wpdm_ap_archive_page');
add_shortcode( 'wpdm_archive', 'wpdm_ap_archive_page');
add_shortcode( 'wpdm-categories', 'wpdm_ap_categories');
add_shortcode( 'wpdm_categories', 'wpdm_ap_categories');
add_shortcode( 'wpdm-tags', 'wpdm_ap_tags');
add_shortcode( 'wpdm_tags', 'wpdm_ap_tags');
add_shortcode( 'wpdm-search-page', 'wpdm_ap_seach_bar');
add_shortcode( 'wpdm_search_page', 'wpdm_ap_seach_bar');
add_action('widgets_init', create_function('', 'return register_widget("wpdm_search_widget");'));
