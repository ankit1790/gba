<?php
/*
Plugin Name: WPDM - Image Button
Description: Use the add-on to replace download link label with a nice button image
Plugin URI: http://www.wpdownloadmanager.com/
Author: Shaon
Version: 2.2.0
Author URI: http://www.wpdownloadmanager.com/
*/

if(defined('WPDM_Version')) {
 

function wpdm_download_buttons(){
    global $current_user, $wpdb;     
     if((isset($_POST['wpdm_dlbtn']) || isset($_POST['wpdm_dlbtn_lt']))&&is_admin()){
         update_option('wpdm_dlbtn',$_POST['wpdm_dlbtn']);
         update_option('wpdm_dlbtn_lt',$_POST['wpdm_dlbtn_lt']);
         _e('Saved!');
         die();
     }
     $data = scandir(dirname(__FILE__).'/images/');
     array_shift($data);
     array_shift($data);
    ?>


    <style type="text/css" xmlns="http://www.w3.org/1999/html">
    
    .btnsl img{
        opacity:0.4;
    }
    .activeb img,
    .btnsl img:hover{
        opacity:1.0;
    }
    .btns{
        opacity:0;
    }
    .btnsl{
        display: block;
        float: left;
        margin: 5px;
        width: 200px;         
    }    
    
.adp-ui-state-highlight{
    width:50px;
    height:50px;
    background: #fff;
    float:left;
    padding: 4px;
    border:1px solid #aaa;
} 
#wpdm-files tbody .ui-sortable-helper{
    width:100%;
    background: #444444;
    
}
#wpdm-files tbody .ui-sortable-helper td{
    color: #fff;
    vertical-align: middle; 
}
input{
   padding: 4px 7px;
}
 
.dfile{background: #ffdfdf;} 
.cfile img, .dfile img{cursor: pointer;}
.inside{padding:10px !important;}
#editorcontainer textarea{border:0px;width:99.9%;}
#icon_uploadUploader,#file_uploadUploader {background: transparent url('<?php echo plugins_url(); ?>/download-manager/images/browse.png') left top no-repeat; }
#icon_uploadUploader:hover,#file_uploadUploader:hover {background-position: left bottom; }
.frm td{line-height: 30px; border-bottom: 1px solid #EEEEEE; padding:5px; font-size:9pt;font-family: Tahoma;}
.fwpdmlock{
    background: #fff;
    border-bottom: 1px solid #eee;
}
.fwpdmlock td{
    border:0px !important;
} 
#filelist {
    margin-top: 10px;
}
#filelist .file{
    margin-top: 5px;
    padding: 0px 10px;   
    color:#444;
    display: block;
    margin-bottom: 5px;
    font-weight: normal;
}

table.widefat{
    border-bottom:0px;
}

.genpass{
    cursor: pointer;
}
 
h3,
h3.handle{
    cursor: default !important;
}


@-webkit-keyframes progress-bar-stripes {
  from {
    background-position: 40px 0;
  }
  to {
    background-position: 0 0;
  }
}

@-moz-keyframes progress-bar-stripes {
  from {
    background-position: 40px 0;
  }
  to {
    background-position: 0 0;
  }
}

@-ms-keyframes progress-bar-stripes {
  from {
    background-position: 40px 0;
  }
  to {
    background-position: 0 0;
  }
}

@-o-keyframes progress-bar-stripes {
  from {
    background-position: 0 0;
  }
  to {
    background-position: 40px 0;
  }
}

@keyframes progress-bar-stripes {
  from {
    background-position: 40px 0;
  }
  to {
    background-position: 0 0;
  }
}

.progress {
  height: 15px;
  margin-bottom: 10px;
  overflow: hidden;
  background-color: #f7f7f7;
  background-image: -moz-linear-gradient(top, #f5f5f5, #f9f9f9);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f5f5f5), to(#f9f9f9));
  background-image: -webkit-linear-gradient(top, #f5f5f5, #f9f9f9);
  background-image: -o-linear-gradient(top, #f5f5f5, #f9f9f9);
  background-image: linear-gradient(to bottom, #f5f5f5, #f9f9f9);
  background-repeat: repeat-x;
  -webkit-border-radius: 4px;
     -moz-border-radius: 4px;
          border-radius: 4px;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5', endColorstr='#fff9f9f9', GradientType=0);
  -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
     -moz-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
          box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
}

.progress .bar {
  float: left;
  width: 0;
  height: 100%;
  font-size: 12px;
  color: #ffffff;
  text-align: center;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #0e90d2;
  background-image: -moz-linear-gradient(top, #149bdf, #0480be);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#149bdf), to(#0480be));
  background-image: -webkit-linear-gradient(top, #149bdf, #0480be);
  background-image: -o-linear-gradient(top, #149bdf, #0480be);
  background-image: linear-gradient(to bottom, #149bdf, #0480be);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff149bdf', endColorstr='#ff0480be', GradientType=0);
  -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
     -moz-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
          box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  -webkit-transition: width 0.6s ease;
     -moz-transition: width 0.6s ease;
       -o-transition: width 0.6s ease;
          transition: width 0.6s ease;
}

.progress .bar + .bar {
  -webkit-box-shadow: inset 1px 0 0 rgba(0, 0, 0, 0.15), inset 0 -1px 0 rgba(0, 0, 0, 0.15);
     -moz-box-shadow: inset 1px 0 0 rgba(0, 0, 0, 0.15), inset 0 -1px 0 rgba(0, 0, 0, 0.15);
          box-shadow: inset 1px 0 0 rgba(0, 0, 0, 0.15), inset 0 -1px 0 rgba(0, 0, 0, 0.15);
}

.progress-striped .bar {
  background-color: #149bdf;
  background-image: -webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255, 255, 255, 0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, 0.15)), color-stop(0.75, rgba(255, 255, 255, 0.15)), color-stop(0.75, transparent), to(transparent));
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  -webkit-background-size: 40px 40px;
     -moz-background-size: 40px 40px;
       -o-background-size: 40px 40px;
          background-size: 40px 40px;
}

.progress.active .bar {
  -webkit-animation: progress-bar-stripes 2s linear infinite;
     -moz-animation: progress-bar-stripes 2s linear infinite;
      -ms-animation: progress-bar-stripes 2s linear infinite;
       -o-animation: progress-bar-stripes 2s linear infinite;
          animation: progress-bar-stripes 2s linear infinite;
}

.progress-danger .bar,
.progress .bar-danger {
  background-color: #dd514c;
  background-image: -moz-linear-gradient(top, #ee5f5b, #c43c35);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#c43c35));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #c43c35);
  background-image: -o-linear-gradient(top, #ee5f5b, #c43c35);
  background-image: linear-gradient(to bottom, #ee5f5b, #c43c35);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffc43c35', GradientType=0);
}

.progress-danger.progress-striped .bar,
.progress-striped .bar-danger {
  background-color: #ee5f5b;
  background-image: -webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255, 255, 255, 0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, 0.15)), color-stop(0.75, rgba(255, 255, 255, 0.15)), color-stop(0.75, transparent), to(transparent));
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
}

.progress-success .bar,
.progress .bar-success {
  background-color: #5eb95e;
  background-image: -moz-linear-gradient(top, #62c462, #57a957);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#57a957));
  background-image: -webkit-linear-gradient(top, #62c462, #57a957);
  background-image: -o-linear-gradient(top, #62c462, #57a957);
  background-image: linear-gradient(to bottom, #62c462, #57a957);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff57a957', GradientType=0);
}

.progress-success.progress-striped .bar,
.progress-striped .bar-success {
  background-color: #62c462;
  background-image: -webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255, 255, 255, 0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, 0.15)), color-stop(0.75, rgba(255, 255, 255, 0.15)), color-stop(0.75, transparent), to(transparent));
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
}

.progress-info .bar,
.progress .bar-info {
  background-color: #4bb1cf;
  background-image: -moz-linear-gradient(top, #5bc0de, #339bb9);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#339bb9));
  background-image: -webkit-linear-gradient(top, #5bc0de, #339bb9);
  background-image: -o-linear-gradient(top, #5bc0de, #339bb9);
  background-image: linear-gradient(to bottom, #5bc0de, #339bb9);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff339bb9', GradientType=0);
}

.progress-info.progress-striped .bar,
.progress-striped .bar-info {
  background-color: #5bc0de;
  background-image: -webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255, 255, 255, 0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, 0.15)), color-stop(0.75, rgba(255, 255, 255, 0.15)), color-stop(0.75, transparent), to(transparent));
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
}

.progress-warning .bar,
.progress .bar-warning {
  background-color: #faa732;
  background-image: -moz-linear-gradient(top, #fbb450, #f89406);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));
  background-image: -webkit-linear-gradient(top, #fbb450, #f89406);
  background-image: -o-linear-gradient(top, #fbb450, #f89406);
  background-image: linear-gradient(to bottom, #fbb450, #f89406);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0);
}

.progress-warning.progress-striped .bar,
.progress-striped .bar-warning {
  background-color: #fbb450;
  background-image: -webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255, 255, 255, 0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, 0.15)), color-stop(0.75, rgba(255, 255, 255, 0.15)), color-stop(0.75, transparent), to(transparent));
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
}
#access{
    width: 250px;
}

#nxt{
  background-color: #C1F4C1;
  background-image: -webkit-gradient(linear, 0 100%, 100% 0, color-stop(0.25, rgba(255, 255, 255, 0.15)), color-stop(0.25, transparent), color-stop(0.5, transparent), color-stop(0.5, rgba(255, 255, 255, 0.15)), color-stop(0.75, rgba(255, 255, 255, 0.15)), color-stop(0.75, transparent), to(transparent));
  background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255, 255, 255, 0.15) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.15) 50%, rgba(255, 255, 255, 0.15) 75%, transparent 75%, transparent);
  -webkit-background-size: 40px 40px;
     -moz-background-size: 40px 40px;
       -o-background-size: 40px 40px;
          background-size: 40px 40px;
  display: none;
  border-bottom:1px solid #008000;   
  color: #0C490C;font-family:'Courier New';padding:5px 10px;text-align: center;
}

#serr{
    display: none;margin-top: 5px;border:1px solid #800000;background: #FFEDED;color: #000;font-family:'Courier New';padding:5px 10px;text-align: left;
}
.action #nxt{
  width:100%;
  position: fixed;  
  top:0px;left:0px;z-index:999999;  
}
#nxt a{
    font-weight: bold;
    color:#0C490C;
}
 
.action-float{
    position:fixed;top:-33px;left:0px;width:100%;z-index:999999;text-align:right;
    background: rgba(0,0,0,0.9);   
}

.action .inside,
.action-float .inside{
    margin: 0px;
}

.action-float #serr{
   width:500px;   
   float: left;
   margin: 4px;
   z-index:999999; 
   margin-top:-50px;
   border:1px solid #800000;    
}
.action-float #nxt{
   width:500px;   
   float: left;
   margin: 4px;
   z-index:999999; 
   margin-top:-40px;
   border:1px solid #008000;   
}

.wpdm-accordion div{
    padding:10px;
}

.wpdmlock {
  opacity:0;  
}
.wpdmlock+label {
   
    width:16px;
    height:16px;
    vertical-align:middle;
}

.wpdm-unchecked{
    display: inline-block;
    float: left;
    width: 21px;
    height: 21px;
    padding: 0px;
    margin: 0px;
    cursor: hand;
    padding: 3px;
    margin-top: -4px !important;
    background-image: url('<?php echo plugins_url('/download-manager/images/CheckBox.png'); ?>');
    background-position: -21px 0px;
}
.wpdm-checked{
    display: inline-block;
    float: left;
    width: 21px;
    height: 21px;
    padding: 0px;
    margin: 0px;
    cursor: hand;
    padding: 3px;
    margin-top: -4px !important;
    background-image: url('<?php echo plugins_url('/download-manager/images/CheckBox.png'); ?>');
    background-position: 0px 0px;
}
.cb-enable, .cb-disable, .cb-enable span, .cb-disable span { background: url(<?php echo plugins_url('/download-manager/images/switch.gif'); ?>) repeat-x; display: block; float: left; }
    .cb-enable span, .cb-disable span { line-height: 30px; display: block; background-repeat: no-repeat; font-weight: bold; }
    .cb-enable span { background-position: left -90px; padding: 0 10px; }
    .cb-disable span { background-position: right -180px;padding: 0 10px; }
    .cb-disable.selected { background-position: 0 -30px; }
    .cb-disable.selected span { background-position: right -210px; color: #fff; }
    .cb-enable.selected { background-position: 0 -60px; }
    .cb-enable.selected span { background-position: left -150px; color: #fff; }
    .switch label { cursor: pointer; }
    .switch input { display: none; }
p.field.switch{
    margin:0px;display:block;float:left;
}
.widefat th{
    font-weight: 700;

}
</style>
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Image Buton</strong></div>


<table id="allbtns" class="table table-striped">
    <thead>
        <tr>
            <th>Button</th>
            <th>Link Template</th>
            <th>Details Page</th>
        </tr>
    </thead>
    <tbody>
    <?php $zc = 0; foreach($data as $btn){ if($btn==get_option('wpdm_dlbtn')) $class = 'activeb'; else $class = '';

        echo "<tr><td><img src='".plugins_url('wpdm-download-button/images/'.$btn)."' /></td><td><input  name='wpdm_dlbtn_lt' type='radio' ".(get_option('wpdm_dlbtn_lt')==$btn?'checked=checked':'')." value='$btn' id='blt".(++$zc)."' /></td><td><input name='wpdm_dlbtn' ".(get_option('wpdm_dlbtn')==$btn?'checked=checked':'')." type='radio' value='$btn' id='b".(++$zc)."' /></td></tr>";
    } ?>

    <tr><td><button type="button" class="btn btn-primary">Link Label</button></td><td><input  name='wpdm_dlbtn_lt' type='radio' <?php echo (get_option('wpdm_dlbtn')=='bs-primary'?'checked=checked':''); ?> value='bs-primary' id='blt<?php echo (++$zc); ?>' /></td><td><input name='wpdm_dlbtn'  <?php echo (get_option('wpdm_dlbtn')=='bs-primary'?'checked=checked':''); ?> value='bs-primary' type="radio" id='b<?php echo (++$zc); ?>' /></td></tr>
    <tr><td><button type="button" class="btn btn-info">Link Label</button></td><td><input  name='wpdm_dlbtn_lt' type='radio' <?php echo (get_option('wpdm_dlbtn')=='bs-info'?'checked=checked':''); ?> value='bs-info' id='blt<?php echo (++$zc); ?>' /></td><td><input name='wpdm_dlbtn' <?php echo (get_option('wpdm_dlbtn')=='bs-info'?'checked=checked':''); ?> value='bs-info' type="radio" id='b<?php echo (++$zc); ?>' /></td></tr>
    <tr><td><button type="button" class="btn btn-warning">Link Label</button></td><td><input  name='wpdm_dlbtn_lt' type='radio' <?php echo (get_option('wpdm_dlbtn')=='bs-warning'?'checked=checked':''); ?> value='bs-warning' id='blt<?php echo (++$zc); ?>' /></td><td><input name='wpdm_dlbtn' <?php echo (get_option('wpdm_dlbtn')=='bs-warning'?'checked=checked':''); ?> value='bs-warning' type="radio" id='b<?php echo (++$zc); ?>' /></td></tr>
    <tr><td><button type="button" class="btn btn-success">Link Label</button></td><td><input  name='wpdm_dlbtn_lt' type='radio' <?php echo (get_option('wpdm_dlbtn')=='bs-success'?'checked=checked':''); ?> value='bs-success' id='blt<?php echo (++$zc); ?>' /></td><td><input name='wpdm_dlbtn' <?php echo (get_option('wpdm_dlbtn')=='bs-success'?'checked=checked':''); ?> value='bs-success' type="radio" id='b<?php echo (++$zc); ?>' /></td></tr>
    <tr><td><button type="button" class="btn btn-danger">Link Label</button></td><td><input  name='wpdm_dlbtn_lt' type='radio' <?php echo (get_option('wpdm_dlbtn')=='bs-danger'?'checked=checked':''); ?> value='bs-danger' id='blt<?php echo (++$zc); ?>' /></td><td><input name='wpdm_dlbtn' <?php echo (get_option('wpdm_dlbtn')=='bs-danger'?'checked=checked':''); ?> value='bs-danger' type="radio" id='b<?php echo (++$zc); ?>' /></td></tr>
    <tr><td><a href="#">Link Label</a></td><td><input  name='wpdm_dlbtn_lt' type='radio' <?php echo (get_option('wpdm_dlbtn')=='linkonly'?'checked=checked':''); ?> value='linkonly' id='blt<?php echo (++$zc); ?>' /></td><td><input name='wpdm_dlbtn' <?php echo (get_option('wpdm_dlbtn')=='linkonly'?'checked=checked':''); ?> type='radio' value='linkonly' id='b<?php echo (++$zc); ?>' /></td></tr>

    </tbody>
 </table>


    </div>
 <div class="clear"></div>
 
  
 
<div class="inside">
  

<div id="plupload-upload-ui" class="hide-if-no-js">
     <div id="drag-drop-area">
       <div class="drag-drop-inside">
        <p class="drag-drop-info"><?php _e('Drop Button Images here'); ?></p>
        <p><?php _ex('or', 'Uploader: Drop Button Images here - or - Select Images'); ?></p>
        <p class="drag-drop-buttons"><input id="plupload-browse-button" type="button" value="<?php esc_attr_e('Select Button Image'); ?>" class="button" /></p>
      </div>
     </div>
  </div>
  
  <?php

  $plupload_init = array(
    'runtimes'            => 'html5,silverlight,flash,html4',
    'browse_button'       => 'plupload-browse-button',
    'container'           => 'plupload-upload-ui',
    'drop_element'        => 'drag-drop-area',
    'file_data_name'      => 'button-async-upload',            
    'multiple_queues'     => true,
    'max_file_size'       => wp_max_upload_size().'b',
    'url'                 => admin_url('admin-ajax.php'),
    'flash_swf_url'       => includes_url('js/plupload/plupload.flash.swf'),
    'silverlight_xap_url' => includes_url('js/plupload/plupload.silverlight.xap'),
    'filters'             => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
    'multipart'           => true,
    'urlstream_upload'    => true,

    // additional post data to send to our ajax hook
    'multipart_params'    => array(
      '_ajax_nonce' => wp_create_nonce('button-upload'),
      'action'      => 'button_upload',            // the ajax action name
    ),
  );

  // we should probably not apply this filter, plugins may expect wp's media uploader...
  $plupload_init = apply_filters('plupload_init', $plupload_init); ?>

  <script type="text/javascript">

    jQuery(document).ready(function($){

      // create the uploader and pass the config from above
      var uploader = new plupload.Uploader(<?php echo json_encode($plupload_init); ?>);

      // checks if browser supports drag and drop upload, makes some css adjustments if necessary
      uploader.bind('Init', function(up){
        var uploaddiv = jQuery('#plupload-upload-ui');

        if(up.features.dragdrop){
          uploaddiv.addClass('drag-drop');
            jQuery('#drag-drop-area')
              .bind('dragover.wp-uploader', function(){ uploaddiv.addClass('drag-over'); })
              .bind('dragleave.wp-uploader, drop.wp-uploader', function(){ uploaddiv.removeClass('drag-over'); });

        }else{
          uploaddiv.removeClass('drag-drop');
          jQuery('#drag-drop-area').unbind('.wp-uploader');
        }
      });

      uploader.init();

      // a file was added in the queue
      uploader.bind('FilesAdded', function(up, files){
        //var hundredmb = 100 * 1024 * 1024, max = parseInt(up.settings.max_file_size, 10);
        
           

        plupload.each(files, function(file){
          jQuery('#filelist').append(
                        '<div class="file" id="' + file.id + '"><b>' +
 
                        file.name + '</b> (<span>' + plupload.formatSize(0) + '</span>/' + plupload.formatSize(file.size) + ') ' +
                        '<div class="progress progress-success progress-striped active"><div class="bar fileprogress"></div></div></div>');
        });

        up.refresh();
        up.start();
      });
      
      uploader.bind('UploadProgress', function(up, file) {
                      
                jQuery('#' + file.id + " .fileprogress").width(file.percent + "%");
                jQuery('#' + file.id + " span").html(plupload.formatSize(parseInt(file.size * file.percent / 100)));
            });
 

      // a file was uploaded 
      uploader.bind('FileUploaded', function(up, file, response) {

        // this is your ajax response, update the DOM with it or something...
        //console.log(response);
        //response
        jQuery('#' + file.id ).remove();
        var d = new Date();
        var ID = d.getTime();
        res = response.response;
        var nm = res;
                            if(response.length>20) nm = response.substring(0,7)+'...'+response.substring(response.length-10);                             
                            jQuery('#allbtns tbody tr:last-child').after("<tr><td><img src='<?php echo plugins_url('wpdm-download-button/images/'); ?>"+res+"' /></td><td><input  name='wpdm_dlbtn_lt' type='radio'  value='"+res+"'  /></td><td><input name='wpdm_dlbtn' type='radio' value='"+res+"' /></td></tr>");
                            //jQuery('#allbtns').append("<input class='btns' name='wpdm_dlbtn' type='radio' value='"+res+"' id='b"+ID+"' /><label class='btnsl {$class}' for='b"+ID+"'><img height='35px' src='<?php echo plugins_url('wpdm-download-button/images/'); ?>"+res+"' /></label>");

                           

      });

    });   

  </script>
  <div id="filelist"></div>

 <div class="clear"></div>
</div>
 
 
 <div class="clear"></div>
  
    <br><br>
    <input name='wpdm_dlbtn' type='radio' value='' /> No Image
    <div style="clear: both;"></div>
    <script language="JavaScript">
    <!--
      jQuery('.btnsl').live('click',function(){
           jQuery('.btnsl').removeClass('activeb');
           jQuery(this).addClass('activeb');            
      });
    //-->
    </script>
    
    <?php
}
 

function wpdm_custom_download_button($package){
    $bsbtns = array(
        'bs-primary' => 'btn btn-primary',
        'bs-info' => 'btn btn-info',
        'bs-warning' => 'btn btn-warning',
        'bs-success' => 'btn btn-success',
        'bs-danger' => 'btn btn-danger',
        'linkonly' => ''
    );
    if(is_single()&&get_post_type()=='wpdmpro'&&$package['ID']==get_the_ID())
        $img  = get_option('wpdm_dlbtn');
    else
        $img  = get_option('wpdm_dlbtn_lt');

    if(in_array($img, array_keys($bsbtns))) $package['download_link'] = str_replace("[btnclass]", $bsbtns[$img], $package['download_link']);

   return $package;
}

function wpdm_apply_image_button($label, $package){

    if(strpos($label, "img src")) return $label;

    $bsbtns = array(
        'bs-primary' => 'btn btn-primary',
        'bs-info' => 'btn btn-info',
        'bs-warning' => 'btn btn-warning',
        'bs-success' => 'btn btn-success',
        'bs-danger' => 'btn btn-danger',
        'linkonly' => ''
    );

    if(is_single()&&get_post_type()=='wpdmpro'&&$package['ID']==get_the_ID())
        $img  = get_option('wpdm_dlbtn');
    else
        $img  = get_option('wpdm_dlbtn_lt');

    if(in_array($img, array_keys($bsbtns))) return $label;

    $imgrl = plugins_url('wpdm-download-button/images/'.$img);
    return "<img src=\"{$imgrl}\" alt=\"{$label}\" />";
}

function wpdm_db_scripts(){
    wp_enqueue_script('plupload-all');
    wp_enqueue_style('plupload-all');
}

function wpdm_button_upload(){
    $tmpdata = explode(".", $_FILES['button-async-upload']['name']);
    $img = uniqid().'.'.end($tmpdata);
    $imgext = explode(".", $img);
    $imgext = end($imgext);
    $imgext = strtolower($imgext);
    if(in_array($imgext, array("jpg","jpeg","png")) && current_user_can("manage_options"))
    move_uploaded_file($_FILES['button-async-upload']['tmp_name'],dirname(__FILE__).'/images/'.$img);
    echo $img;
    die();
}

add_action('wpdm_button_image','wpdm_apply_image_button',10,2);
add_action('wp_ajax_button_upload','wpdm_button_upload');
add_action('admin_enqueue_scripts','wpdm_db_scripts');
add_filter('download_link','wpdm_custom_download_button');
//add_filter('wdm_before_fetch_template','wpdm_custom_download_button');
add_wdm_settings_tab('download-button', 'Image Button', 'wpdm_download_buttons');
}