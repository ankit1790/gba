<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<title>GBA News Letter</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=4.0, user-scalable=yes">
<link type="image/x-icon" rel="shortcut icon" href="http://104.130.31.87/wp-content/uploads/2015/07/favicon.ico">

</head>

<body style="margin:0; font-family: 'Oswald', sans-serif; font-size: 16px;">
<table width="100%" border="0">
  <tr>
    <td colspan="3"><h1 align="center" style="color: #0084c9; font-size: 5vw; letter-spacing: 5px; padding: 25px 0 0; line-height:0; padding-bottom:20px;">LETTER FROM INEZ</h1></td>
  </tr>
  <tr>
    <td colspan="3" style=" background:url(http://104.130.31.87/wp-content/uploads/2015/07/globe.png) no-repeat center top; background-size: 86% auto; position: relative; text-align:center;">
    
    <div class="header-content" style="background: #aec250;  margin-bottom:-1px; bottom: 0; color: #fff; left: 0; padding:1% 0 2%; margin-top: 30%; width: 100%;">

	  <div class="sm-container" style="margin: 0 10px;">
			<h2 class="text-uppercase" style="  font-size: 2vw; letter-spacing: 3px; padding-bottom: 10px; color:#fff;">Women's Banking Service is a Growth Market</h2>
			<p style="font-size: 1.8vw; color:#fff;">This month I was invited to speak at consultations at the... The correlation between access to financial services for women and gender equality is based on empirical evidence, and that evidence is part of the calculus of assessing </p>
			<a href="#" class="read-more" style="background: rgba(0, 0, 0, 0) url(http://104.130.31.87/wp-content/uploads/2015/07/read-icon.png) no-repeat scroll 13px 9px;    border: 1px solid #fff;    border-radius: 25px;    color: #fff;    display: inline-block;    font-size: 15px;    font-weight: 300;    margin-top: 15px;    padding: 11px 28px 11px 72px;
">READ MORE</a>
		</div>
	</div>
    
    </td>
  </tr>
  <tr>
    <td colspan="3"><div class="vital text-center" style="padding: 0; text-align: center; background:#fff;">	
	<div class="container" style="margin: 4% auto; width: 86%;">
		<h2 class="text-uppercase" style="color: #2c3a69; font-size: 2vw; margin:2%; text-transform: uppercase;">vital <span style="color: #a5bc3e; padding-left: 7px;">STATISTICS:</span></h2
        
        ><ul style="margin: 0;padding: 0;">
			<li style="color: #a9cb1f; font-size: 4vw; line-height: 1; font-weight: 300; padding: 10px 0;display: inline-block;   padding: 10px 6.5% 10px 5.5%;list-style: none;">45</li>
			<li style="color: #a9cb1f; font-size: 4vw; line-height: 1; font-weight: 300; padding: 10px 0; display: inline-block;   padding: 10px 6.5% 10px 5.5%;list-style: none; background: url(http://104.130.31.87/wp-content/uploads/2015/07/disc.png) no-repeat left center; ">27</li>
			<li style="color: #a9cb1f; font-size: 4vw;; line-height: 1; font-weight: 300; padding: 10px 0; display: inline-block;   padding: 10px 6.5% 10px 5.5%;list-style: none; background: url(http://104.130.31.87/wp-content/uploads/2015/07/disc.png) no-repeat left center;">3,421</li>
		</ul>
        
        
	</div>	
</div></td>
  </tr>
  <tr>
    <td colspan="3" style="background:url(http://104.130.31.87/wp-content/uploads/2015/07/feature_banner.jpg) no-repeat center top;  background-size: 100% auto; 
  padding: 0; position: relative; text-align:center;">
  
  	<h1 class="text-uppercase" style="  color: #fff; font-size: 4vw; letter-spacing: 5px; padding: 25px 0 0;">1ST FEATURED STORY TITLE</h1>

   <div class="feature-content" style=" background: #024696; bottom: 0; color: #fff; left: 0; padding: 1% 0 2% 0; margin-top:30%; width: 100%;">
	  <div class="sm-container" style="  margin: 0 20px;; text-align: center;">
			<h2 class="text-uppercase" style="font-size: 2vw; color:#fff;">Women's Banking Service is a Growth Market</h2>
			<p style="font-size: 1.8vw; color:#fff;">This month I was invited to speak at consultations at the... The correlation between access to financial services for women and gender equality is based on empirical evidence, and that evidence is part of the calculus of assessing </p>
			<a href="#" class="read-more" style="
    border: 1px solid #fff;    border-radius: 25px;    color: #fff;    display: inline-block;    font-size: 15px;    font-weight: 300;    margin-top: 15px;    padding: 11px 28px 11px 28px;
    text-decoration: none;
"><img src="http://104.130.31.87/wp-content/uploads/2015/07/read-icon.png" style="margin-right:20px; float:left;">READ MORE</a>
		</div>
	 </div>

  
  
  </td>
  </tr>
  <tr>
    <td colspan="3"><div class="content-column" style="margin-top:5%; margin-bottom:5%;">	
	<div class="container" style="margin: 0 auto; width: 86%; display: flex;">
		<ul style="display: flex; margin: 0; padding: 0;  display: flex;">
			<li style="width: 31%; margin: 0 2px; display: inline-block; list-style: none;">
				<img src="http://104.130.31.87/wp-content/uploads/2015/07/img-1.jpg" style="width: 100%; display: block; padding: 0 0 20px;" alt=""/>
				<h3 style="font-size: 1.7vw;">Westpac Announces July Study Tour</h3>
				<p style="color: #919399; font-size: 1.4vw; font-weight: 300; padding: 20px 0 0;">Bank al Etihad of Jordan recently announced the launch of its Shorouq Program, which offers a wide variety of financial and non-financial services for women. The bank's</p>
				<a href="#" style="font-size: 18px; color: #8acb37;text-decoration: none;">Read More</a>
			</li>
			<li style="width: 31%;margin: 0 2px; display: inline-block;list-style: none;">
				<img src="http://104.130.31.87/wp-content/uploads/2015/07/img-2.jpg" style="width: 100%; display: block; padding: 0 0 20px;"  alt=""/>
				<h3 style="font-size: 1.7vw;">New Member Spotlight: Banco Vision</h3>
				<p style="color: #919399; font-size: 1.4vw; font-weight: 300; padding: 20px 0 0">The Global Banking Alliance for Women announces a partnership with the Inter-American Development Bank and Data2X to expand the collection and use…</p>
				<a href="#" style="font-size: 18px; color: #8acb37;text-decoration: none;">Read More</a>
			</li>
			<li style="width: 31%; margin: 0 2px;
    display: inline-block; list-style: none;">
				<img src="http://104.130.31.87/wp-content/uploads/2015/07/img-3.jpg" style="width: 100%; display: block; padding: 0 0 20px;" alt=""/>
				<h3 style="font-size: 1.7vw;">GBA Summit to feature Data Symposium</h3>
				<p style="color: #919399; font-size: 1.4vw; font-weight: 300; padding: 20px 0 0;">IFC is helping Bank al Etihad launch a new banking model that caters to women in Jordan, with a particular focus on small and medium enterprises owned by women.</p>
				<a href="#" style="font-size: 18px; color: #8acb37;text-decoration: none;">Read More</a>

			</li>
		</ul>
	</div>	
</div></td>
  </tr>
  <tr>
    <td colspan="3" style="background:url(http://104.130.31.87/wp-content/uploads/2015/07/resource_banner.jpg) no-repeat center top; background-size: 86% auto; padding: 0 0 0px; position: relative;">
    	<h1 align="center" class="text-uppercase" style="color: #333; padding-top:2%; text-transform: uppercase; font-size: 4vw; letter-spacing: 5px;    padding:  0 0;">RESOURCE OF THE MONTH</h1>

    <div class="resource text-center" style="padding: 0 0 5%; position: relative; text-align: center;">
     <a href="#" class="read-more" style="border: 1px solid #fff; margin-top: 18%; border-radius: 25px; color: #fff; display: inline-block; font-size: 15px;    font-weight: 300;padding: 11px 28px 11px 28px; background-color: #fd7d0b; text-decoration: none;"><img src="http://104.130.31.87/wp-content/uploads/2015/07/read-icon.png" style="margin-right:20px; float:left;">READ MORE</a>
  
</div>


    
    </td>
  </tr>
  
  <tr>
  <td colspan="3"><div class="resource-content" style="background: #fd7d0b;bottom: 0;color: #fff;left: 0; padding:2%; margin: 5% 0 3%; text-align:center;
">		
	  <h2 class="text-uppercase" style="text-transform: uppercase;
    font-size: 30px; letter-spacing: 3px; padding-bottom: 10px;
    margin: 0; padding: 0;
">Upcoming events</h2>
	</div></td>
  </tr>
  
  <tr>
    <td colspan="3"><div class="sponsor text-center" style="padding: 4% 0 30px;
  text-align: center;">
	<h2 class="text-uppercase" style="text-transform: uppercase; color: #2c3a69; font-size: 30px;    letter-spacing: 3px;    padding-bottom: 10px;
">our <span style="  color: #a5bc3e;
  padding-left: 7px;">sponsors</span></h2>
	<ul style="  padding: 2% 0;">
		<li style="padding: 0 2.6% 30px; vertical-align: middle; display: inline-block; list-style: none;">
        <img src="http://104.130.31.87/wp-content/uploads/2015/07/scotia.png" style="width:100%;" alt=""/></li>
		<li style="padding: 0 2.6% 1%; vertical-align: middle; display: inline-block; list-style: none;"><img src="http://104.130.31.87/wp-content/uploads/2015/07/standard.png" style="width:100%;" alt=""/></li>
		<li style="padding: 0  2.6% 1%; vertical-align: middle; display: inline-block; list-style: none;"><img src="http://104.130.31.87/wp-content/uploads/2015/07/RBS.png" style="width:100%; alt=""/></li>
		<li style="padding: 0 2.6% 1%; vertical-align: middle; display: inline-block; list-style: none;"><img src="http://104.130.31.87/wp-content/uploads/2015/07/aus.png" style="width:100%; alt=""/></li>
		<li style="padding: 0  2.6% 1%; vertical-align: middle; display: inline-block; list-style: none;"><img src="http://104.130.31.87/wp-content/uploads/2015/07/diamond.png" style="width:100%; alt=""/></li>
		<li style="padding: 0  2.6% 1%; vertical-align: middle; display: inline-block; list-style: none;"><img src="http://104.130.31.87/wp-content/uploads/2015/07/ocbc.png" style="width:100%; alt=""/></li>
	</ul>	
</div></td>
  </tr>
  <tr>
    <td colspan="3"><div class="footer text-center" style="background: #024696; color: #fff; padding: 10px 0 45px; text-align: center;">	
	<ul class="links text-uppercase" style="padding: 0 0 30px; text-transform: uppercase;">
		<li style="  padding: 0 3.8% 0 3.5%; display: inline-block; list-style: none; color: #fff;">
<a href="#" style="border-bottom: 1px solid transparent; color: #fff; font-size: 13px; font-weight: 400; letter-spacing: 1px; padding: 2px; text-decoration: none;">Welcome</a><img src="http://104.130.31.87/wp-content/uploads/2015/07/divider.png" style="float: right; "></li>
		<li style="  padding: 0 3.8% 0 3.5%; display: inline-block; list-style: none; color: #fff;"><a href="#" style="border-bottom: 1px solid transparent; color: #fff; font-size: 13px; font-weight: 400; letter-spacing: 1px; padding: 2px; text-decoration: none;">About GBA</a><img src="http://104.130.31.87/wp-content/uploads/2015/07/divider.png" style="float: right;"></li>
		<li style="  padding: 0 3.8% 0 3.5%; display: inline-block; list-style: none; color: #fff;"><a href="#" style="border-bottom: 1px solid transparent; color: #fff; font-size: 13px; font-weight: 400; letter-spacing: 1px; padding: 2px; text-decoration: none;"> Our WOrk</a><img src="http://104.130.31.87/wp-content/uploads/2015/07/divider.png" style="float: right; "></li>
		<li style="  padding: 0 3.8% 0 3.5%; display: inline-block; list-style: none; color: #fff;"><a href="#" style="border-bottom: 1px solid transparent; color: #fff; font-size: 13px; font-weight: 400; letter-spacing: 1px; padding: 2px; text-decoration: none;">News &amp; Events</a><img src="http://104.130.31.87/wp-content/uploads/2015/07/divider.png" style="float: right;"></li>
		<li style="  padding: 0 3.8% 0 3.5%;display: inline-block; list-style: none; color: #fff;"><a href="#" style="border-bottom: 1px solid transparent; color: #fff; font-size: 13px; font-weight: 400; letter-spacing: 1px; padding: 2px; text-decoration: none;">Join</a><img src="http://104.130.31.87/wp-content/uploads/2015/07/divider.png" style="float: right; "></li>
		<li style="  padding: 0 3.8% 0 3.5%;display: inline-block; list-style: none; color: #fff;"><a href="#" style="border-bottom: 1px solid transparent; color: #fff; font-size: 13px; font-weight: 400; letter-spacing: 1px; padding: 2px; text-decoration: none;">Resources</a><img src="http://104.130.31.87/wp-content/uploads/2015/07/divider.png" style="float: right; "></li>
		<li style="  padding: 0 3.8% 0 3.5%;display: inline-block; list-style: none; color: #fff;"><a href="#" style="border-bottom: 1px solid transparent; color: #fff; font-size: 13px; font-weight: 400; letter-spacing: 1px; padding: 2px; text-decoration: none;">Careers</a><img src="http://104.130.31.87/wp-content/uploads/2015/07/divider.png" style="float: right; "></li>
		<li style="  padding: 0 3.8% 0 3.5%;display: inline-block; list-style: none; color: #fff;"><a href="#" style="border-bottom: 1px solid transparent; color: #fff; font-size: 13px; font-weight: 400; letter-spacing: 1px; padding: 2px; text-decoration: none;">Contact</a></li>
	</ul>

	<p style="font-size: 14px; font-weight: 300; letter-spacing: 1px; color:#fff;">Global Banking Alliance for Women, 540 President Street, Brooklyn, NY 11215<br>
&copy; Copyright 2014 Global Banking Alliance, All rights reserved.</p>

	<ul class="social" style="padding: 30px 0 0;">
		<li style="display: inline-block; list-style: none; padding: 0 5px;">
        <a href="#" class="facebook"><img src="http://104.130.31.87/wp-content/uploads/2015/07/facebook.png"></a></li>
		<li style="display: inline-block; list-style: none; padding: 0 5px;">
        <a href="#" class="twitter"><img src="http://104.130.31.87/wp-content/uploads/2015/07/twitter.png"></a></li>
		<li style="display: inline-block; list-style: none; padding: 0 5px;">
        <a href="#" class="google"><img src="http://104.130.31.87/wp-content/uploads/2015/07/google-plus.png"></a></li>
		<li style="display: inline-block; list-style: none; padding: 0 5px;">
        <a href="#" class="linkedin"><img src="http://104.130.31.87/wp-content/uploads/2015/07/linkdin.png"></a></li>
		<li style="display: inline-block; list-style: none; padding: 0 5px;">
        <a href="#" class="utube"><img src="http://104.130.31.87/wp-content/uploads/2015/07/youtube.png"></a></li>
	</ul>
  </div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
</table>

</body>
</html>
